import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/
//import { IndexComponent } from './welcome/index/index.component'

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { WelcomeRouting }  from './welcome/welcome.routing'
import { AuthenticationRouting }  from './authentication/authentication.routing'
import { Tienda724Routing }  from './tienda724/tienda724.routing'


import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';


export const AppRouting: Routes = [
  // { path: '',          redirectTo: '', pathMatch: 'full' }
  ...WelcomeRouting,
  ...AuthenticationRouting,
  ...Tienda724Routing,
  { path: '*', redirectTo:'/',pathMatch:'full'}
];
