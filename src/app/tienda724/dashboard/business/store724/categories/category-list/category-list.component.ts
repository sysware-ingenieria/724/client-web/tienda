import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { TreeModule, TreeNode } from 'primeng/primeng';

import { AccordionModule } from 'primeng/primeng';     //accordion and accordion tab
import { MenuItem } from 'primeng/primeng';            //api

import { Token } from '../../../../../../shared/token';




import { LocalStorage } from '../../../../../../shared/localStorage';
import { CategoriesService } from '../categories.service';
import { ProductsService } from '../../products/products.service'



import { Category } from '../models/category';
import { Product } from '../../products/models/product';

import { MatTabChangeEvent, VERSION } from '@angular/material';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  childrenAux: TreeNode[];
  categoryTree: TreeNode[];
  files: TreeNode[];
  selectedFile: TreeNode;

  current_index_productor: number;

  id_category_productor: number;

  isMyThird = true;
  token: Token;
  ID_THIRD_TYPE: number;

  CURRENT_ID_THIRD = 0;

  version = VERSION;
  @ViewChild('tabGroup') tabGroup;
  categoryList: Category[];
  productors: Category[];
  productorsTabs: Category[];
  childsByProductor: Category[];
  STATE = 1

  productList: Product[];

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public categoriesService: CategoriesService,
    public productService: ProductsService) {
    this.productors = [];
    this.childsByProductor = [];
    this.productorsTabs = [];
    this.categoryTree = [];
    this.current_index_productor = 0;
    this.id_category_productor = 0;

  }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }


  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD = this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD !== null && this.CURRENT_ID_THIRD > 0) {

        this.getProductList()


      }

      this.categoriesService.getFiles().then(files => this.files = files);
      this.getCategory(this.STATE, null, null, null, null, null, null, null, null, null)


    }



  }

  getProductList(state_product?: number, id_product?: number, id_category?: number, stock?: number, stock_min?: number,
    img_url?: string, code?: string, id_tax?: number, id_common_product?: number, name_product?: number,
    description_product?: string, id_state_product?: number) {

    this.productService.getProductList(state_product, null, id_category)
      .subscribe((data: Product[]) => this.productList = data,
      error => console.log(error),
      () => {


      });

  }


  // GET /Thirds
  getCategory(state: number, id_category: number, id_common: number, id_category_father: number, img_url: string, name: string,
    description: string, id_state: number, creation_attribute: Date, modify_attribute: Date): void {


    this.categoriesService.getCategoryList(state, id_category, id_common, id_category_father, img_url, name,
      description, id_state, creation_attribute, modify_attribute)

      .subscribe((data: Category[]) => this.categoryList = data,
      error => console.log(error),
      () => {

        if (this.categoryList.length > 0) {

          this.productors = _.filter(this.categoryList, function (element) { return element.id_category_father === 0; });
          this.categoryList = _.filter(this.categoryList, function (element) { return element.id_category_father !== 0; });


        }



      });

  }
  count = 0
  loadCategory(id_category) {




  }

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {


    this.current_index_productor = tabChangeEvent.index
    this.childsByProductor = [];
    this.loadChildsByProductor(this.current_index_productor);
  }


  loadChildsByProductor(current_index_productor) {
    if (this.productors.length > 0) {
      this.categoryTree = [];
      for (let x of this.categoryList) {
        this.childrenAux = [];
        if (x.id_category_father === this.productors[current_index_productor].id_category) {
          this.childsByProductor.push(x);
          for (let c of this.categoryList) {
            if (c.id_category_father === x.id_category) {

              this.childrenAux.push(
                {
                  label: c.common.name,
                  data: c
                }
              );

            }
          }
          this.categoryTree.push(
            {
              label: x.common.name,
              data: x,
              children: this.childrenAux,

            }
          );
        }


      }

    }
  }

  id_categAux = 0;
  venozolana(selectedFile) {

    if (selectedFile !== undefined && selectedFile !== null) {
      console.log("entro ?");
      this.getProductList(this.STATE, null, selectedFile.data.id_category)
      this.id_categAux = selectedFile.data.id_category
    }
  }

  addProduct() {

    if (this.id_categAux !== 0) {
      alert("se va a crear un producto de la seleccion " + this.id_categAux);
      this._router.navigate(['/dashboard/business/product/new'], { queryParams: { id_category: this.id_categAux } });
    } else {

      alert("se va a crear un producto para productores " + this.productors[this.current_index_productor].id_category);

      this.id_category_productor = this.productors[this.current_index_productor].id_category
      this._router.navigate(['/dashboard/business/product/new'], { queryParams: { id_category: this.id_category_productor } });
    }
  }

  addProductor(){
    this._router.navigate(['/dashboard/business/category/new']);
    
  }
  addSub(){
    if (this.id_categAux !== 0) {
      alert("se va a crear una subcategoria de la categoria " + this.id_categAux);
      this._router.navigate(['/dashboard/business/category/new'], { queryParams: { father: this.id_categAux } });
    } else {

      alert("se va a crear una categoria para el productor " + this.productors[this.current_index_productor].id_category);

      this.id_category_productor = this.productors[this.current_index_productor].id_category
      this._router.navigate(['/dashboard/business/category/new'], { queryParams: { father: this.id_category_productor } });
    }
  }


}
