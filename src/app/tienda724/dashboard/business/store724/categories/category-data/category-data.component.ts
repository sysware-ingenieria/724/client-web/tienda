import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';



import {MatTabChangeEvent, VERSION} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { CategoriesService } from '../categories.service';
import { ProductsService } from '../../products/products.service'


import { Category } from '../models/category';
import { Product } from '../../products/models/product';

var categoryList:Category[];
var categoryListGlobal:Category[];

@Component({
  selector: 'app-category-data',
  templateUrl: './category-data.component.html',
  styleUrls: ['./category-data.component.scss']
})
export class CategoryDataComponent implements OnInit {

  isMyThird=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  
  thirdAux:Category[];

  productList:Product[];






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:CategoryDataSource


  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public categoryService: CategoriesService,
    public productService:ProductsService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      

        this.getCategory(this.STATE)
        

      
      

    }
  }

  getCategory(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

    this.categoryService.getCategoryList(state)
    .subscribe((data: Category[]) => categoryList = data,
    error => console.log(error),
    () => {

      this.dataSource = new CategoryDataSource();
    });

  }


  getProductList(state_product?:number,id_product?:number,id_category?:number,stock?:number,stock_min?:number,
    img_url?:string,code?:string,id_tax?:number,id_common_product?:number,name_product?:number,
    description_product?:string,id_state_product?:number){
    
        this.productService.getProductList()
        .subscribe((data: Product[]) => this.productList= data,
        error => console.log(error),
        () => {
        
            console.log(this.productList)
         
        });
    
      }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  editCategory(category:Category){ 
    this._router.navigate(['/dashboard/business/category/edit',category.id_category] ); 
 
 
 
  } 

  addCategory(category:Category){ 
    
    this._router.navigate(['/dashboard/business/category/new'],{queryParams:{father:category.id_category}} ); 
 
  } 

  deleteCategory(id_category) {
    
        this.categoryService.Delete(id_category)
          .subscribe(
          result => {
            
            if (result === true) {
              categoryList = _.filter(categoryList, function (f) { return f.id_category !== id_category; });
              this.dataSource = new CategoryDataSource();
            
              alert("Eliminado correctamente");
    
              return;
            } else {
              //this.openDialog();
              return;
            }
          })
    
      }


}
export class CategoryDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<Category[]> {

    return Observable.of(categoryList);
  }

  disconnect() {}
}
