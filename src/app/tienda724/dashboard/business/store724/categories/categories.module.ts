import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import {AccordionModule} from 'primeng/primeng';     //accordion and accordion tab
import {MenuItem} from 'primeng/primeng';            //api
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TreeModule,TreeNode} from 'primeng/primeng';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryDataComponent } from './category-data/category-data.component';
import { CategoryNewComponent } from './category-new/category-new.component';
import { CategoryEditComponent } from './category-edit/category-edit.component';

/*
************************************************
*     modules of  your app
*************************************************
*/

//import {   } from "../products";

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { CategoriesService } from './categories.service';
import { ProductsService } from '../products/products.service'
/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    TreeModule
  ],
  declarations: [CategoryListComponent, CategoryDataComponent, CategoryNewComponent, CategoryEditComponent],
  providers:[CategoriesService,ProductsService]
})
export class CategoriesModule { }
