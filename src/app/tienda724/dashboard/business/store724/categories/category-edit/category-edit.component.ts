import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { CategoryDTO } from '../models/categoryDTO'
import { Category } from '../models/category'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'



/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { CategoriesService } from '../categories.service';
/*
*     constant of  your component
*/
declare var $:any
@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.scss']
})
export class CategoryEditComponent implements OnInit {
  
  form: FormGroup;
  token:Token;
  newCategory:CategoryDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;

   //attributes
   CURRENT_ID_THIRD = 0;
   CURRENT_ID_CATEGORY = 0;
   ID_CATEGORY = 0;
   STATE=1;

   category:Category[]
   currentCategory:Category

  constructor(private router: Router, private route:ActivatedRoute ,private categoriesService: CategoriesService,
              private fb: FormBuilder, private locStorage: LocalStorage) {

                this.common=new CommonStoreDTO(null,null)
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newCategory=new CategoryDTO(null,null,null,this.common,this.commonStateStoreDTO)
              }

 

  ngOnInit() {
    this.createControls();
    
        let session=this.locStorage.getSession();
        if(!session){
            this.Login();
        }else{

          this.token=this.locStorage.getToken();
          this.route.params
          .subscribe( params =>{
              this.ID_CATEGORY=+params['id']

                
            // Defaults to 0 if no query param provided.  
            this.getCategory(this.STATE,this.ID_CATEGORY)


          });
          
          this.route.queryParams
              .subscribe(params => {
    
                // Defaults to 0 if no query param provided.  
                  this.token=this.locStorage.getToken();
          
                  this.CURRENT_ID_CATEGORY=params.father;
                  
                   
            });
        }
  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  getCategory(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.categoriesService.getCategoryList(state)
        .subscribe((data: Category[]) => this.category = data,
        error => console.log(error),
        () => {

          if(this.category.length>0){
            this.currentCategory=this.category[0]
            this.loadData()
          }
    
         
        });
    
      }


  createControls() {
    this.form = this.fb.group({

      //profile
      img_url: ['', Validators.compose([
        
      ])],
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  loadData() {
    this.form.patchValue({
    
      img_url:""+ this.currentCategory.img_url,
      name: this.currentCategory.common.name,
      description:this.currentCategory.common.description
    });
  
  }

  createNewCategory() {
    
        this.common.name= this.form.value["name"];
        this.common.description= this.form.value["description"];
        this.newCategory.id_category_father = this.currentCategory.id_category_father>0?this.currentCategory.id_category_father:null;
        this.newCategory.id_third_category=this.CURRENT_ID_THIRD>0?this.CURRENT_ID_THIRD:null;
        this.newCategory.img_url= this.form.value["img_url"];
        this.newCategory.common=this.common;
        this.newCategory.state=this.commonStateStoreDTO;
    
        console.log("CAE ",this.newCategory)
        this.categoriesService.putMeasureUnit(this.ID_CATEGORY,this.newCategory)
        .subscribe(
        result => {
         
          if (result === true) {
             this.resetForm();
             this.goBack();
           
    
    
            return;
          } else {
    
            //this.openDialog();
            return;
          }
        })
      }

      showNotification(from, align,id_type?, msn?){
        const type = ['','info','success','warning','danger'];
    
        const color = Math.floor((Math.random() * 4) + 1);
    
        $.notify({
            icon: "notifications",
            message: msn?msn:"<b>Noficación automatica </b>"
    
        },{
            type: type[id_type?id_type:2],
            timer: 200,
            placement: {
                from: from,
                align: align
            }
        });
      }
    
      resetForm() {
        
        this.form.reset();
        
      }
    
      goBack() {
        let link = ['/dashboard/business/category/data'];
        this.router.navigate(link);
      }

}
