import { TestBed, inject } from '@angular/core/testing';

import { ProductThirdService } from './product-third.service';

describe('ProductThirdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductThirdService]
    });
  });

  it('should be created', inject([ProductThirdService], (service: ProductThirdService) => {
    expect(service).toBeTruthy();
  }));
});
