import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { ProductThirdListComponent } from './product-third-list/product-third-list.component';
import { ProductThirdDataComponent } from './product-third-data/product-third-data.component';
import { ProductThirdNewComponent } from './product-third-new/product-third-new.component';
import { ProductThirdEditComponent } from './product-third-edit/product-third-edit.component';
import { ProductThirdDetailComponent } from './detail/product-third-detail.component'


/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const ProductsThirdRouting: Routes = [
  
    { path: 'product-third', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: ProductThirdListComponent},
          { path: 'data', component: ProductThirdDataComponent},
          { path: 'new', component: ProductThirdNewComponent},
          { path: 'detail/:id', component: ProductThirdDetailComponent},
          { path: 'edit/:id', component: ProductThirdEditComponent}
  
      ]
    }
  
  ]



