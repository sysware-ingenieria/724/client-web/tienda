import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { ProductThird } from './models/productThird'
import { ProductThirdDTO } from './models/productThirdDTO'

@Injectable()
export class ProductThirdService {

  api_uri = Urlbase[2] + '/products-third';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }


  public getProductThirdList = (state?: number, id_category?: number, id_common?: number, id_category_father?: number, img_url?: string, name?: string,
    description?: string, id_state?: number, creation_attribute?: Date, modify_attribute?: Date): Observable<{} | ProductThird[]> => {

    console.log("id_category")
    let params: URLSearchParams = new URLSearchParams();
    params.set('id_product_third', id_category ? "" + id_category : null);
    params.set('id_common', id_common ? "" + id_common : null);
    params.set('id_category_father', id_category_father ? "" + id_category_father : null);
    params.set('img_url', img_url ? "" + img_url : null);
    params.set('name', name ? "" + name : null);
    params.set('description', description ? "" + description : null);
    params.set('id_state', id_state ? "" + id_state : null);
    params.set('state_prod_third', state ? "" + state : null);
    params.set('creation_attribute', creation_attribute ? "" + creation_attribute : null);
    params.set('modify_attribute', modify_attribute ? "" + modify_attribute : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri, this.options)
      .map((response: Response) => <ProductThird[]>response.json())
      .catch(this.handleError);
  }

  public Delete = (id_category: number): Observable<Response | any> => {
    return this.http.delete(this.api_uri + '/' + id_category, this.options)
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  public postCategory = (body: ProductThirdDTO, id_code?: number): Observable<Number | any> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_code', id_code ? "" + id_code : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;

    return this.http.post(this.api_uri, body, this.options)
      .map((response: Response) => {
        if (response) {
          let id=+response["_body"]
          return id;
        } else {

          return null;
        }
      })
      .catch(this.handleError);
  }
public putProductThird = (id:number, body: ProductThirdDTO, code?:String): Observable<Boolean|any> =>{
  return this.http.put(this.api_uri +"/"+ id, body, { headers: this.headers })
  .map((response: Response) => {
        if(response){
          return true;
        }else{

      return false;
    }
  })
  .catch(this.handleError);
}

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

}
