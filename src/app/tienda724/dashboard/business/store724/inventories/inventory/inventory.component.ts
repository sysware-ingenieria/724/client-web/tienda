  import { Component, OnInit } from '@angular/core';
  import { DataSource } from '@angular/cdk/collections';
  import { Router } from '@angular/router';
  import { Observable } from 'rxjs/Observable';
  import 'rxjs/add/observable/of';
  import * as _ from 'lodash';



  import {MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


  import { LocalStorage } from '../../../../../../shared/localStorage';
  import { Token } from '../../../../../../shared/token';

  import { InventoriesService } from '../inventories.service';
  import { BarCodeService } from '../../bar-codes/bar-code.service';
  import { AttributeService } from '../../attributes/attribute.service';


  import { AttributeDetailList} from '../../attributes/models/attributeDetailList'
  import { AttributeValue } from '../../attributes/models/attributeValue'
  import { Third } from '../../../thirds724/third/models/third';
  

  import { Code } from '../../bar-codes/models/code';


import { InventoryDetail } from '../models/inventoryDetail'
  import { Inventory } from '../models/inventory';
  import { DialogQuantityComponent } from '../dialog-quantity/dialog-quantity.component'


  var inventoryList:InventoryDetail[];
  var inventoryListGlobal:InventoryDetail[];


  import { FilterInventory } from '../models/filters'

  @Component({
    selector: 'app-inventory',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.scss']
  })
  export class InventoryComponent implements OnInit {

    isMyProduct=true;
    token:Token;
    ID_THIRD_TYPE: number;
    STATE=1
    CURRENT_ID_THIRD = 0;
    CURRENT_ID_THIRD_PATHER=0;

    thirdFathet:Third;
    inventoryListAux:Inventory[];
    attributeDetailList:AttributeDetailList[]
    selectValues:AttributeValue[]  ;   
    attributeValueList:AttributeValue[]  ;    
    
    
    thirdAux:Inventory[];

    codeList:Code[];
    id_codeList:number[];
    inventories: Inventory[];
    



    displayedColumns = ['position', 'name', 'weight', 'location', 'symbol', 'direccion'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:InventoryDataSource;

    filterInventory:FilterInventory;
    

    constructor(public locStorage: LocalStorage,
      private _router: Router,
      public barCodeService:BarCodeService,
      public attributeService:AttributeService,
      public inventoriesService: InventoriesService, public dialog: MatDialog) { 
        inventoryList=[]
        this.selectValues
        this.attributeValueList=[]
        this.inventories=[]
        this.filterInventory= new FilterInventory(null,null,null,null,null,null,null,null,null,null,null,this.attributeValueList);

      }




    ngOnInit() {

      let session = this.locStorage.getSession();
      if (!session) {
        this.Login();
      } else {
        this.token = this.locStorage.getToken();
        this.CURRENT_ID_THIRD=this.token.id_third;
        this.CURRENT_ID_THIRD_PATHER=this.token.id_third_father;
        this.ID_THIRD_TYPE = 23;

        if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

          this.getInventoryID(this.STATE,null,this.CURRENT_ID_THIRD_PATHER)
          
        } 
        

      }

    }

    getInventoryID(state?:number,id_inventory?:number,id_third?:number){
      
        this.inventoriesService.getInventoriesList(state,id_inventory,id_third)
          .subscribe((data: Inventory[]) => this.inventories = data,
          error => console.log(error),
          () => {
            if(this.inventories.length>0){
              let id_inv=this.inventories[0].id_inventory
              if(id_inv>0){
                console.log("tiene invenntario")
                this.getInventoryList(this.STATE, null,null, id_inv, null, null, this.CURRENT_ID_THIRD_PATHER)
                  
                
              }
              
  
            }else{
              alert("No tiene inventario")
             
            }
          });
  }
  

    showFilter(event):void{
      
      this.filterInventory=event;

      let datas
      let ID_INVENTORY=-1
      this.inventoriesService.postInventoriesDetailListFilters(this.CURRENT_ID_THIRD_PATHER,ID_INVENTORY,this.filterInventory)
          .subscribe((data: InventoryDetail[]) => inventoryList = data,
          error => console.log(error),
          () => {
            console.log("RESPUESTA INMEDIATA "+inventoryList)    
            this.dataSource = new InventoryDataSource();
                     // this.getInventoryList(this.STATE,null,null,null,null,null,this.CURRENT_ID_THIRD_PATHER)

          });

      //this.beginFilters()
            
    }

    beginFilters(){
      inventoryList=[]
      this.dataSource = new InventoryDataSource();
          if(this.filterInventory.id_cat_productor!==null){
        
      
              this.getInventoryList(this.STATE,null,null,null,null,null,this.filterInventory.id_venue,
                this.filterInventory.id_category_th,null,null,null,this.filterInventory.id_cat_productor,null,
                null,null,null,null,null,null,null,null,null,this.filterInventory.id_measure_unit,null,null,
                null,null,null,null,null,null,null,null,null,null,null,null);
      
            
          }else{
      
              this.getInventoryList(this.STATE,null,null,null,null,null,this.filterInventory.id_venue,
                this.filterInventory.id_category_th,null,null,null,this.filterInventory.id_productor,null,
                null,null,null,null,null,null,null,null,null,this.filterInventory.id_measure_unit,null,null,
                null,null,null,null,null,null,null,null,null,null,null,null);
          }
    }


    Login() {
      let link = ['/auth'];
      this._router.navigate(link);
    }

    getInventoryList(state_inv_detail?:number,state_product?:number,id_inventory_detail?:number, 
      id_inventory?:number, id_product_third?:number,location?:number,
      id_third?:number,id_category_third?:number, quantity?:number,id_state_inv_detail?:number,
      id_product?:number,id_category?:number,stock?:number,
      stock_min?:number,img_url?:string,id_tax?:number,
      id_common_product?:number,name_product?:string,
      description_product?:string,id_state_product?:number,
    

      id_state_prod_third?:number,state_prod_third?:number,
      id_measure_unit?:number,id_measure_unit_father?:number,
      id_common_measure_unit?:number,name_measure_unit?:string,
      description_measure_unit?:string,id_state_measure_unit?:number,
      state_measure_unit?:number,id_code?:number,
      code?:number,img?:string,
      id_attribute_list?:number,
      id_state_cod?:number,state_cod?:number,
      attribute?:number,
      attribute_value?:number){


      if((attribute!==null && attribute>0)  || (attribute_value!==null && attribute_value>0)){
      
        this.loadInventorAttr(state_inv_detail,state_product,id_inventory_detail, 
          id_inventory, id_product_third,location,
          id_third,id_category_third, quantity,id_state_inv_detail,
          id_product,id_category,stock,
          stock_min,img_url,id_tax,
          id_common_product,name_product,
          description_product,id_state_product,
        
      
          id_state_prod_third,state_prod_third,
          id_measure_unit,id_measure_unit_father,
          id_common_measure_unit,name_measure_unit,
          description_measure_unit,id_state_measure_unit,
          state_measure_unit,id_code,
          code,img,
          id_attribute_list,
          id_state_cod,state_cod,
          attribute,
          attribute_value);
      }else{
          this.inventoriesService.getInventoriesDetailList(state_inv_detail,state_product,id_inventory_detail, 
                                                            id_inventory, quantity,id_state_inv_detail,
                                                            id_product,id_category,stock,
                                                            stock_min,img_url,id_tax,
                                                            id_common_product,name_product,
                                                            description_product,id_state_product,
                                                            id_product_third,location,
                                                            id_third,id_category_third,
                                                  
                                                            id_state_prod_third,state_prod_third,
                                                            id_measure_unit,id_measure_unit_father,
                                                            id_common_measure_unit,name_measure_unit,
                                                            description_measure_unit,id_state_measure_unit,
                                                            state_measure_unit,id_code,
                                                            code,img,
                                                            id_state_cod,state_cod)


          .subscribe((data: InventoryDetail[]) => inventoryList = data,
          error => console.log(error),
          () => {
      
            this.dataSource = new InventoryDataSource();
          }); 
        } 
      }

      loadInventorAttr(state_inv_detail?:number,state_product?:number,id_inventory_detail?:number, 
      id_inventory?:number, id_product_third?:number,location?:number,
      id_third?:number,id_category_third?:number, quantity?:number,id_state_inv_detail?:number,
      id_product?:number,id_category?:number,stock?:number,
      stock_min?:number,img_url?:string,id_tax?:number,
      id_common_product?:number,name_product?:string,
      description_product?:string,id_state_product?:number,
    

      id_state_prod_third?:number,state_prod_third?:number,
      id_measure_unit?:number,id_measure_unit_father?:number,
      id_common_measure_unit?:number,name_measure_unit?:string,
      description_measure_unit?:string,id_state_measure_unit?:number,
      state_measure_unit?:number,id_code?:number,
      code?:number,img?:string,
      id_attribute_list?:number,
      id_state_cod?:number,state_cod?:number,
      attribute?:number,
      attribute_value?:number){

    
          
        this.attributeService.getAttributeDetailList(null,null,null,null,attribute_value,attribute)
            .subscribe((data: AttributeDetailList[]) => this.attributeDetailList = data,
            error => console.log(error),
              () => {
                inventoryList=[]
                for(let object of this.attributeDetailList){

                  alert(object.id_attribute_list,)

                  this.inventoriesService.getInventoriesDetailList(state_inv_detail,state_product,id_inventory_detail, 
                    id_inventory, quantity,id_state_inv_detail,
                    id_product,id_category,stock,
                    stock_min,img_url,id_tax,
                    id_common_product,name_product,
                    description_product,id_state_product,
                    id_product_third,location,
                    id_third,id_category_third,
            
                    id_state_prod_third,state_prod_third,
                    id_measure_unit,id_measure_unit_father,
                    id_common_measure_unit,name_measure_unit,
                    description_measure_unit,id_state_measure_unit,
                    state_measure_unit,id_code,
                    code,img,
                    object.id_attribute_list,
                    id_state_cod,state_cod)
                  .subscribe((data: InventoryDetail[]) =>  inventoryList.concat(data),
                  error => console.log(error),
                  () => {
                    console.log("LISTA-> ",inventoryList)

                  
                    this.dataSource = new InventoryDataSource();
                  });


                }

                this.dataSource = new InventoryDataSource();

              
          
                
          });
          
            


        




      
      
    }
    changeQuantity(element){

      let dialogRef = this.dialog.open(DialogQuantityComponent, {
        height: '280px',
        width: '290px',
        data: { quantityTemp:element
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.beginFilters()
        console.log(result);
      });

    }

  }

  export class InventoryDataSource extends DataSource<any> {
    /** Connect function called by the table to retrieve one stream containing the data to render. */

    connect(): Observable<InventoryDetail[]> {

      return Observable.of(inventoryList);
    }

    disconnect() {}
  }
