import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { InventoryComponent } from './inventory/inventory.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const InventoryRouting: Routes = [
  
    { path: 'inventory', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: InventoryComponent}
  
      ]
    }
  
  ]