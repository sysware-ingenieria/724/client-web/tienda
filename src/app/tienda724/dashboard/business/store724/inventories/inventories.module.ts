import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { InventoryFilterComponent } from './inventory-filter/inventory-filter.component';
import { InventoryComponent } from './inventory/inventory.component';
import { DetailComponent } from './inventory/detail/detail.component';

/*
************************************************
*     modules of  your app
*************************************************
*/


/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { InventoriesService } from './inventories.service';
import { DialogQuantityComponent } from './dialog-quantity/dialog-quantity.component';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  declarations: [InventoryFilterComponent, InventoryComponent, DetailComponent, DialogQuantityComponent],
  entryComponents: [DialogQuantityComponent],
  providers:[InventoriesService],
  exports:[InventoryComponent]
})
export class InventoriesModule { }
