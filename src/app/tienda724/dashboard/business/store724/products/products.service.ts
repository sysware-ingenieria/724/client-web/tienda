import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Product } from './models/product'
import { ProductDTO } from './models/productDTO'

@Injectable()
export class ProductsService {

  
  api_uri = Urlbase[2] + '/products';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');
    
        this.options = new RequestOptions({headers:this.headers});
  }


  public getProductList = (state_product?:number,id_product?:number,id_category?:number,stock?:number,stock_min?:number,
    img_url?:string,code?:string,id_tax?:number,id_common_product?:number,name_product?:number,
    description_product?:string,id_state_product?:number ): Observable<{}|Product[]> => {

      
      let params: URLSearchParams = new URLSearchParams();
     
        params.set('id_product',  id_product?""+id_product:null);
        params.set('id_category',  id_category?""+id_category:null);
        params.set('stock', stock?""+stock:null);
        params.set('stock_min', stock_min?""+stock_min:null);
        params.set('img_url', img_url?""+img_url:null);
        params.set('code', code?""+code:null);
        params.set('id_tax', id_tax?""+id_tax:null);
        params.set('id_common_product', id_common_product?""+id_common_product:null);
        params.set('name_product', name_product?""+ name_product:null);
        params.set('description_product', description_product?""+description_product:null);
  
        params.set('id_state_product', id_state_product?""+id_state_product:null);
        params.set('state_product', state_product?""+state_product:null);
        
      
    
        
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri, this.options )
        .map((response: Response) => <Product[]>response.json())
        .catch(this.handleError);
    }

    public Delete = (id_product: number): Observable<Response|any> => {
      return this.http.delete(this.api_uri +'/'+ id_product,this.options)
      .map((response: Response) => {
        if(response){
          return true;
        }else{
  
      return false;
        }})
          .catch(this.handleError);
    }

    public postProduct = (body: ProductDTO): Observable<Boolean|any> => {
      
        return this.http.post(this.api_uri , body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }



    public putProduct = (id:number,body: ProductDTO): Observable<Boolean|any> => {
      
      return this.http.put(this.api_uri +"/"+ id, body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }

    private handleError(error: Response | any) {
      // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      alert(errMsg);
      return null;//Observable.throw(errMsg);
    }


}
