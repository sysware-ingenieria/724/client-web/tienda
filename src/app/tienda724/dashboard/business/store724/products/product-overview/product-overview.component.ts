import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

/*
*    Material modules for component
*/

/*
*     others component
*/
/*
*     models for  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage';
import { Code } from '../../bar-codes/models/code';
import { Product } from '../../products/models/product';
import { MeasureUnit } from '../../measure-unit/models/measureUnit';
import { Attribute } from '../../attributes/models/attribute';
import { AttributeList } from '../../attributes/models/attributeList';
import { AttributeDetailList } from '../../attributes/models/attributeDetailList';
import { AttributeValue } from '../../attributes/models/attributeValue';
import { Category } from '../../categories/models/category';
import { Token } from '../../../../../../shared/token';
/*
*     services of  your component
*/
import { ProductsService } from '../../products/products.service';
import { CategoriesService } from '../../categories/categories.service';
import { MeasureUnitService } from '../../measure-unit/measure-unit.service';
import { AttributeService } from '../../attributes/attribute.service';

/*
*     constant of  your component
*/


@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.scss']
})
export class ProductOverviewComponent implements OnInit {
  // Inputs

  @Input()
  ID_CODE:number
  @Input()
  code:string
  // Outputs
  @Output()
  suggested_priceEmit=new EventEmitter();

  suggested_price:number;
  

  productList:Product[];
  currentProduct:Product;



  constructor(private productService:ProductsService,
              private categoriesService:CategoriesService,
              private measureUnitService:MeasureUnitService,
              private attributeService:AttributeService) { 
    this.logNameChange();
    this.getProductList(1)

  }

  ngOnInit() {
    
    console.log("CODE -> ", this.code);
    console.log("ID CODE -> ", this.ID_CODE);
    this.eventEmit()

  }

  eventEmit(){
    this.suggested_price=2345;
    this.suggested_priceEmit.emit(this.suggested_price);
  }

  logNameChange(){
   
  }

  // Services

  getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.productService.getProductList(state)
        .subscribe((data: Product[]) => this.productList = data,
        error => console.log(error),
        () => {
          if(this.productList.length>0){
            this.currentProduct= this.productList[0];

            console.log("EMITIR YA ")
            this.eventEmit();
          }
          
        });
    
      }

}
