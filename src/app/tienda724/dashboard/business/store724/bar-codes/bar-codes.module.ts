import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { BarCodeListComponent } from './bar-code-list/bar-code-list.component';
import { BarCodeNewComponent } from './bar-code-new/bar-code-new.component';
import { BarCodeEditComponent } from './bar-code-edit/bar-code-edit.component';

/*
************************************************
*     modules of  your app
*************************************************
*/

//import {   } from "../products";

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { BarCodeService } from './bar-code.service'
import { ProductsService } from '../products/products.service'
import { MeasureUnitService } from '../measure-unit/measure-unit.service'
import { AttributeService } from '../attributes/attribute.service'

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,

  ],
  declarations: [BarCodeListComponent, BarCodeNewComponent, BarCodeEditComponent],
  providers:[BarCodeService,ProductsService,MeasureUnitService,AttributeService]
})
export class BarCodesModule { }
