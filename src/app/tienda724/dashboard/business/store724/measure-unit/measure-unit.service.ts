import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { MeasureUnit } from './models/measureUnit'
import { MeasureUnitDTO } from './models/measureUnitDTO'


@Injectable()
export class MeasureUnitService {

  api_uri = Urlbase[2] + '/measure-units';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');
    
        this.options = new RequestOptions({headers:this.headers});
  }

  public getCategoryList = (state_measure_unit?: number,id_measure_unit?:number,id_measure_unit_father?:number,
    id_common_measure_unit?:number, name_measure_unit?: string, 
    description_measure_unit?:string, id_state_measure_unit?:number, creation_measure_unit?: Date, modify_measure_unit?:Date): Observable<{}|MeasureUnit[]> => {

      
      let params: URLSearchParams = new URLSearchParams();
      params.set('id_measure_unit',  id_measure_unit?""+id_measure_unit:null);
      params.set('id_measure_unit_father',  id_measure_unit_father?""+id_measure_unit_father:null);
      params.set('id_common_measure_unit', id_common_measure_unit?""+id_common_measure_unit:null);
      params.set('name_measure_unit', name_measure_unit?""+name_measure_unit:null);
      params.set('description_measure_unit', description_measure_unit?""+description_measure_unit:null);
      params.set('id_state_measure_unit', id_state_measure_unit?""+id_state_measure_unit:null);
      params.set('state_measure_unit', state_measure_unit?""+state_measure_unit:null);
      params.set('creation_measure_unit', creation_measure_unit?""+creation_measure_unit:null);
      params.set('modify_measure_unit', modify_measure_unit?""+modify_measure_unit:null);
        
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri, this.options )
        .map((response: Response) => <MeasureUnit[]>response.json())
        .catch(this.handleError);
    }

    public Delete = (id_category: number): Observable<Response|any> => {
      return this.http.delete(this.api_uri +'/'+ id_category,this.options)
      .map((response: Response) => {
        if(response){
          return true;
        }else{
  
      return false;
        }})
          .catch(this.handleError);
    }

    public postCategory = (body: MeasureUnitDTO): Observable<Boolean|any> => {
      
        return this.http.post(this.api_uri , body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }

    

    public putMeasureUnit = (id:number,body: MeasureUnitDTO): Observable<Boolean|any> => {
      
      return this.http.put(this.api_uri +"/"+ id, body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }



  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

}
