import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeThirdComponent } from './type-third.component';

describe('TypeThirdComponent', () => {
  let component: TypeThirdComponent;
  let fixture: ComponentFixture<TypeThirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeThirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
