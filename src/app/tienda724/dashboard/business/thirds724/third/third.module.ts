import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { ThirdDataComponent } from './third-data/third-data.component';
import { NewThirdComponent } from './new-third/new-third.component';
import { EditThirdComponent } from './edit-third/edit-third.component';
/*
************************************************
*     modules of  your app
*************************************************
*/
import {  DocumentTypeModule } from "../document-type/document-type.module";

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { DocumentTypeService } from '../document-type/document-type.service';
import { ThirdService } from './third.service';
import { AuthenticationService } from '../../../../../authentication/authentication.service';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    DocumentTypeModule
  ],
  declarations: [  ThirdDataComponent,
                  NewThirdComponent,
                  EditThirdComponent],
  providers:[DocumentTypeService, ThirdService, AuthenticationService]
})
export class ThirdModule { }
