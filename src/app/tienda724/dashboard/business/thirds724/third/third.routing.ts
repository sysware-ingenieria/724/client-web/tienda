import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { ThirdDataComponent } from './third-data/third-data.component';
import { NewThirdComponent } from './new-third/new-third.component';
import { EditThirdComponent } from './edit-third/edit-third.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const ThirdRouting: Routes = [

  { path: 'third', component: null,
    children: [
        { path: '', redirectTo: 'list', pathMatch: 'full'},
        { path: 'list', component: ThirdDataComponent},
        { path: 'new', component: NewThirdComponent},
        { path: 'edit/:id', component: EditThirdComponent}

    ]
  },

]
