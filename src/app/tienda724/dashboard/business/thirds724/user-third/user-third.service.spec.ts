import { TestBed, inject } from '@angular/core/testing';

import { UserThirdService } from './user-third.service';

describe('UserThirdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserThirdService]
    });
  });

  it('should be created', inject([UserThirdService], (service: UserThirdService) => {
    expect(service).toBeTruthy();
  }));
});
