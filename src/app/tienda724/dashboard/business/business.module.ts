import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


/*
************************************************
*    Material modules for app
*************************************************
*/
import {  MaterialModule } from '../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import {  TypeThirdComponent } from "./thirds724/type-third/type-third.component";
import {  DashboardComponent } from "../../../dashboard/dashboard.component";

/*
************************************************
*     modules of  your app
*************************************************
*/
import {  ThirdModule } from "./thirds724/third/third.module";
import {  DocumentTypeModule } from "./thirds724/document-type/document-type.module";
import {  CategoriesModule } from "./store724/categories/categories.module";
import {  BarCodesModule } from "./store724/bar-codes/bar-codes.module";
import {  MeasureUnitModule } from "./store724/measure-unit/measure-unit.module";
import {  ProductsModule } from "./store724/products/products.module";
import {  ProductsThirdModule } from "./store724/products-third/products-third.module";
import {  AttributesModule } from "./store724/attributes/attributes.module";
import {  InventoriesModule }  from './store724/inventories/inventories.module'
import {  Billing724Module }  from './billing724/Billing724.module'



/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { LocalStorage } from '../../../shared/localStorage';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ThirdModule,
    DocumentTypeModule,
    CategoriesModule,
    BarCodesModule,
    MeasureUnitModule,
    ProductsModule,
    ProductsThirdModule,
    AttributesModule,
    InventoriesModule,
    Billing724Module

  ],
  providers:[LocalStorage],
  declarations: [TypeThirdComponent,DashboardComponent],
  exports:[DashboardComponent]
})
export class BusinessModule { }
