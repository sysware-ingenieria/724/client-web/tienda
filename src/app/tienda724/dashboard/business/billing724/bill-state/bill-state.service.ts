import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';

/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { BillState } from './models/billState'
/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillStateDTO } from './models/billStateDTO'
import { Bill } from 'app/tienda724/dashboard/business/billing724/billing/models/bill';


@Injectable()
export class BillStateService {
  api_uri = Urlbase[3] + '/billing-state';


  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }

  public getInventoriesList = (state_inventory?: number, id_inventory?: number,
    id_third?: number, id_common_inventory?: number,
    name_inventory?: string, description_inventory?: string,
    id_state_inventory?: number): Observable<{} | Bill[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    params.set('id_third', id_third ? "" + id_third : null);
    params.set('id_common_inventory', id_common_inventory ? "" + id_common_inventory : null);
    params.set('name_inventory', name_inventory ? "" + name_inventory : null);
    params.set('description_inventory', description_inventory ? "" + description_inventory : null);
    params.set('id_state_inventory', id_state_inventory ? "" + id_state_inventory : null);
    params.set('state_inventory', state_inventory ? "" + state_inventory : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri, this.options)
      .map((response: Response) => <Bill[]>response.json())
      .catch(this.handleError);
  }



  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }


}
