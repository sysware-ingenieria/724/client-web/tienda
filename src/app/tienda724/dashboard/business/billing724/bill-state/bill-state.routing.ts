import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { BillStateDataComponent } from './bill-state-data/bill-state-data.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const BillStateRouting: Routes = [
  
    { path: 'bill-state', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: BillStateDataComponent},
         
  
      ]
    }
  
  ]