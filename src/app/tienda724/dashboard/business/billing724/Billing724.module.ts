import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccordionModule} from 'primeng/primeng';     //accordion and accordion tab
import {MenuItem} from 'primeng/primeng';            //api
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TreeModule,TreeNode} from 'primeng/primeng';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { OptionsComponent } from './options/options.component';


/*
************************************************
*     modules of  your app
*************************************************
*/

import { WayToPayModule } from './way-to-pay/way-to-pay.module';
import { PaymentStateModule } from './payment-state/payment-state.module';
import { PaymentMethodModule } from './payment-method/payment-method.module';
import { BillingModule } from './billing/billing.module';
import { BillTypeModule } from './bill-type/bill-type.module';
import { BillStateModule } from './bill-state/bill-state.module';
import { DocumentModule } from './document/document.module';

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    WayToPayModule,
    PaymentStateModule,
    PaymentMethodModule,
    BillingModule,
    BillTypeModule,
    BillStateModule,
    DocumentModule
    
  ],
  declarations: [OptionsComponent],
  exports:[OptionsComponent],
  providers:[]
})
export class Billing724Module { }
