import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/*
*     others component
*/

/*
*     models for  your component
*/
import { BillType } from '../bill-type/models/billType';
import { Token } from '../../../../../shared/token'


/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../shared/localStorage'

import { BillTypeService } from '../bill-type/bill-type.service';
/*
*     constant of  your component
*/

declare var $:any;
@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {
   // attribute
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER=0;
  STATE=1
  

    // models
    token:Token;
    //lists
    billType:BillType[];
    SubMenu:any[];

  constructor(public locStorage: LocalStorage,private _router: Router,private billTypeService:BillTypeService) {
    this.SubMenu=[]

   }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER=this.token.id_third_father;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){
           this.getBillType();
      } 
    }
  }


  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

   // Services
   getBillType(STATE?:number){

    this.billTypeService.getBillTypeResource(STATE)
    .subscribe((data: BillType[]) =>  this.billType=data,
    error => console.log(error),
    () => {
      if(this.billType.length>0){

        for(let obj of this.billType){
          
          if(obj.name_bill_type.toLocaleLowerCase().includes('compra')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"euro_symbol",
                "route":"/dashboard/business/movement/billing/shop"
              }
            )          
          }
          if(obj.name_bill_type.toLocaleLowerCase().includes('venta')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"add_shopping_cart",
                "route":"/dashboard/business/movement/billing/sale",
                "color":"orange"
              }
            )
           
          }
          if(obj.name_bill_type.toLocaleLowerCase().includes('entrada')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"card_giftcard",
                "route":"/dashboard/business/movement/billing/input",
                "color":""
              }
            )
          }
          if(obj.name_bill_type.toLocaleLowerCase().includes('salida')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"favorite_border",
                "route":"/dashboard/business/movement/billing/output",
                "color":""
              }
            )
          }
          
          if(obj.name_bill_type.toLocaleLowerCase().includes('remision')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"exit_to_app",
                "route":"/dashboard/business/movement/billing/remission",
                "color":""
              }
            )
          }
          if(obj.name_bill_type.toLocaleLowerCase().includes('devolución')){
            this.SubMenu.push(
              {"id":obj.id_bill_type,
                "name":obj.name_bill_type,
                "icon":"keyboard_return",
                "route":"/dashboard/business/movement/billing/return",
                "color":""
              }
            )
          }
                      
        }

      }else{
        alert(" NO SE PUEDE GENERAR UNA FACTURA [TIPO DE FACTURA]")
      }
    });
  }

}
