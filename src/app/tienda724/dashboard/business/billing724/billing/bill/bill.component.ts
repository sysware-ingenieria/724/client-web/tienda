import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*
*     others component
*/

/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { Bill } from '../models/bill'
import { CommonState } from '../../commons/commonState'
import { Document } from '../../commons/document'
import { CommonStateDTO } from '../../commons/commonStateDTO'
import { DocumentDTO } from '../../commons/documentDTO'
import { BillType } from '../../bill-type/models/billType';
import { Third } from '../../../thirds724/third/models/third';
import { Code } from '../../../store724/bar-codes/models/code';
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail';
import { Inventory } from '../../../store724/inventories/models/inventory';
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'
import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';



/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { BillingService } from '../billing.service';
import { BillTypeService } from '../../bill-type/bill-type.service';
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { DocumentService } from '../../document/document.service';
import { ThirdService } from '../../../thirds724/third/third.service'
import { BarCodeService } from '../../../store724/bar-codes/bar-code.service';

/*
*     constant of  your component
*/

declare var $: any;
@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {
  // attribute
  //flags
  isVenueThird = false;
  isExit = true;
  isCode = false;
  isDetail = false;
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER = 0;

  STATE = 1
  total = 0
  today = Date.now();
  TOTAL_PRICE = 0;





  // models
  form: FormGroup;
  token: Token;

  //lists
  bill: Bill[]
  billType: BillType[];
  inventoryList: InventoryDetail[];

  inventoryDetailForBilling: InventoryDetail[];
  thirdListList: Third[];
  third: Third;
  thirdStores: Third[];
  thirdStoreVenue: Third[];
  itemLoadBilling: InventoryDetail;
  // DTO's
  detailBillingDTOList: any[]
  commonStateDTO: CommonStateDTO;
  documentDTO: DocumentDTO;
  inventoryQuantityDTO: InventoryQuantityDTO;
  inventoryQuantityDTOList: InventoryQuantityDTO[];
  detailBillDTO: DetailBillDTO;
  detailBillDTOList: DetailBillDTO[];
  billDTO: BillDTO;



  constructor(public locStorage: LocalStorage, private fb: FormBuilder,
    private _router: Router, public dialog: MatDialog,
    private billingService: BillingService, private billTypeService: BillTypeService,
    public inventoriesService: InventoriesService,
    public documentService: DocumentService,
    public thirdService: ThirdService,
    public barCodeService: BarCodeService) {

    this.createControls();
    this.logNameChange();
    this.loadData();
    this.thirdStoreVenue = []
    this.inventoryList = []
    this.inventoryDetailForBilling = []
    this.detailBillingDTOList = []
    this.inventoryQuantityDTOList = [];

    this.detailBillDTOList = [];

    this.commonStateDTO = new CommonStateDTO(1, null, null);
    this.documentDTO = new DocumentDTO(null, null);
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null)
    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO);
    this.billDTO = new BillDTO(null, null, null, null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList, this.documentDTO);

  }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.third = this.locStorage.getThird();
      this.CURRENT_ID_THIRD = this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER = this.token.id_third_father;

      if (this.CURRENT_ID_THIRD !== null && this.CURRENT_ID_THIRD > 0) {

        this.getBillType();

        this.getThird(true, this.STATE, null, this.CURRENT_ID_THIRD)
        this.getThird(true, this.STATE, null, this.CURRENT_ID_THIRD_PATHER)
        this.getInventoryList(this.STATE, null, null, null, null, null, this.CURRENT_ID_THIRD_PATHER)
      }


    }
  }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  // loadDatil()
  loadDatailBilling(element) {

    if (this.form.value['quantity'] > 0) {
      console.log("DISSS-> ", element)
      this.TOTAL_PRICE = this.TOTAL_PRICE + (+(this.form.value['quantity']) * element.description.standard_price)
      if (this.detailBillingDTOList.length > 0) {
        for (let i = 0; i < this.detailBillingDTOList.length; i++) {
          console.log("ITEM-> ", this.detailBillingDTOList[i])

          if (this.detailBillingDTOList[i].item.detail.id_inventory_detail === element.detail.id_inventory_detail) {
            this.detailBillingDTOList[i].item.detail.quantity = this.detailBillingDTOList[i].item.detail.quantity + (+this.form.value['quantity'])

          } else {
            this.detailBillingDTOList.push(
              { 'item': element, 'quantity': this.form.value['quantity'] }
            )
          }
        }

      } else {

        this.detailBillingDTOList.push(
          { 'item': element, 'quantity': this.form.value['quantity'] }
        )
      }
    } else {
      this.showNotification('top', 'center', 3, "<h3>Ingrese la cantidad</h3> ", 'warning')
    }

    if (this.detailBillingDTOList.length > 0) {
      this.isDetail = true;
    } else {
      this.isDetail = false;
    }

    this.loadDataAfterDetail();


  }

  // start controls
  createControls() {
    this.form = this.fb.group({

      id_th_venue: ['', Validators.compose([
        Validators.required
      ])],
      date: ['', Validators.compose([
        Validators.required
      ])],
      quantity: ['', Validators.compose([

      ])],
      billType: ['', Validators.compose([
        Validators.required
      ])],
      codeProd: ['', Validators.compose([
        Validators.required
      ])],
      title: ['', Validators.compose([
        Validators.required
      ])],
      body: ['', Validators.compose([
        Validators.required
      ])],
    });

  }
  loadData() {
    this.form.patchValue({
      date: new Date()
    });
  }
  loadDataAfterDetail() {
    this.form.patchValue({
      quantity: 0,
      codeProd: ''

    });

    this.total = 0;
  }

  logNameChange() {

    const id_billTypeControl = this.form.get('billType');
    id_billTypeControl.valueChanges.forEach((value: string) => {

      let billType = id_billTypeControl['value'];
      if (billType) {

        this.isCode = true;
        if (billType.name_bill_type.toLowerCase() === "VENTA".toLowerCase()) { // IF Active all
          console.log(" VENTA ", billType);
          this.isVenueThird = false;
          this.isExit = true;
          this.isDetail = false;
          this.itemLoadBilling = null;
          this.detailBillingDTOList = [];
          this.loadDataAfterDetail();


        }
        if (billType.name_bill_type.toLowerCase() === "COMPRA".toLowerCase()) { // IF Active all
          console.log(" COMPRA ", billType);
          this.isVenueThird = false;
          this.isExit = false;
          this.isDetail = false;
          this.itemLoadBilling = null;
          this.detailBillingDTOList = [];
          this.loadDataAfterDetail();

        }
        if (billType.name_bill_type.toLowerCase() === "DEVOLUCION".toLowerCase()) { // IF Active all
          console.log(" DEVOLUCION ", billType);
          this.isVenueThird = false;
          this.isExit = false;
          this.isDetail = false;
          this.itemLoadBilling = null;
          this.detailBillingDTOList = [];
          this.loadDataAfterDetail();

        }
        if (billType.name_bill_type.toLowerCase() === "REMISION".toLowerCase()) { // IF Active all
          console.log(" REMISION ", billType);
          if (this.thirdStoreVenue.length > 0) {
            this.isExit = true;
            this.isVenueThird = true;
            this.isDetail = false;
            this.itemLoadBilling = null;
            this.detailBillingDTOList = [];
            this.loadDataAfterDetail();

          } else {
            this.showNotification('top', 'center', 3, "No Tiene sedes a donde efectruar el movimiento", 'warning')
          }

        }


      }
    });
    const dateControl = this.form.get('date')
    dateControl.valueChanges.forEach((value: string) => {

      let date: number = dateControl['value'];

      if (date) { // IF Active all
        console.log(" SEDE CMABIO ", date);

      }
    }
    );

    const codeProdControl = this.form.get('codeProd');
    codeProdControl.valueChanges.forEach((value: string) => {


      this.itemLoadBilling = codeProdControl['value'];
      console.log(" PROD", codeProdControl);

      if (this.itemLoadBilling) { // IF Active all
        console.log(" SEDE CMABIO ", this.itemLoadBilling);
        this.total = this.itemLoadBilling.detail.quantity;

      }
    });

    const quantityControl = this.form.get('quantity');
    quantityControl.valueChanges.forEach((value: string) => {


      let tempQuanty: number = quantityControl['value'];
      console.log(" PROD", quantityControl);

      if (tempQuanty) { // IF Active all


        if (this.isExit) {
          this.total = this.itemLoadBilling.detail.quantity - (+tempQuanty);

        } else {
          this.total = this.itemLoadBilling.detail.quantity + (+tempQuanty);

        }

      }
    });





  }
  // end controls

  // end process
  save() {

    console.log("INICIA PROCESO DE, ", this.form.value['billType']);
    this.commonStateDTO.state = 1
    let id_inventory = null
    this.inventoryQuantityDTOList = []
    let case_1 = true;// origen
    let case_2 = true;// Destino
    let id_third_venue = null
    this.documentDTO.title = 'Movimiento tipo ' + this.form.value['billType'].name_bill_type
    this.documentDTO.body = 'TEST';


    /**
     * building detailBill and Quantity
     */
    for (let element of this.detailBillingDTOList) {
      this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null);
      id_inventory = element.item.detail.id_inventory;

      //building detailBill
      this.detailBillDTO.price = 0
      this.detailBillDTO.tax = 0
      this.detailBillDTO.id_product_third = element.item.description.id_product_third
      this.detailBillDTO.tax_product = 0
      this.detailBillDTO.state = this.commonStateDTO;
      console.log("ELEM ", element)

      //building Quantity for discount tienda

      //Detail billing
      this.detailBillDTO.quantity = element.quantity;
      this.detailBillDTOList.push(this.detailBillDTO);


      // Inv Quantity
      this.inventoryQuantityDTO.id_inventory_detail = element.item.detail.id_inventory_detail;
      this.inventoryQuantityDTO.id_product_third = element.item.description.id_product_third;
      this.inventoryQuantityDTO.code = element.item.detail.code;

      // Inv Quantity for discount tienda
      this.inventoryQuantityDTO.quantity = element.quantity;

      this.inventoryQuantityDTOList.push(
        this.inventoryQuantityDTO
      )


    }

    if (this.detailBillDTOList.length > 0) {
      this.billDTO.id_third_employee = this.token.id_third;
      this.billDTO.id_third = this.token.id_third_father;
      this.billDTO.id_bill_type = this.form.value['billType'].id_bill_type;
      this.billDTO.id_bill_state = 1;
      this.billDTO.purchase_date = this.form.value['date'];
      this.billDTO.subtotal = 0;
      this.billDTO.tax = 0;
      this.billDTO.totalprice = 0;
      this.billDTO.discount = 0;
      this.billDTO.documentDTO = this.documentDTO;
      this.billDTO.state = this.commonStateDTO;



      this.billDTO.details = this.detailBillDTOList;
      this.billingService.postBillResource(this.billDTO)
        .subscribe(
        result => {
          if (result) {

            this.beginPlusOrDiscount(id_inventory, this.inventoryQuantityDTOList, this.isExit)
          }
        });
    }




  }
  buyProccess() {

  }
  saleProccess() {

  }

  // Notifications

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  // Services
  getBillType(STATE?: number) {
    this.billTypeService.getBillTypeResource(STATE)
      .subscribe((data: BillType[]) => this.billType = data,
      error => console.log(error),
      () => {
        if (this.billType.length > 0) {

        } else {
          alert(" NO SE PUEDE GENERAR UNA FACTURA [TIPO DE FACTURA]")
        }
      });
  }

  getThird(is_venue: boolean, state?: number, id_third?: number, id_third_father?: number, document_type?: number, document_number?: string, id_doctype_person?: number, doc_person?: string, id_third_type?: number, id_person?: number): void {

    this.thirdService.getThirdList(id_third, id_third_father, document_type, document_number,
      id_doctype_person, doc_person, id_third_type, state, id_person)
      .subscribe((data: Third[]) => this.thirdListList = data,
      error => console.log(error),
      () => {
        this.prosessingDataThird(is_venue);
      });
  }

  prosessingDataThird(is_venue: boolean) {

    if (is_venue) {


      for (let element of this.thirdListList) {


        if ((element.profile === null || element.profile === 0) && (element.id_third_father === this.CURRENT_ID_THIRD || element.id_third_father === this.CURRENT_ID_THIRD_PATHER)) {
          if (this.thirdStoreVenue.length > 0) {
            for (let objet of this.thirdStoreVenue) {

              if (element.id_third !== objet.id_third) {

                this.thirdStoreVenue.push(element);
              }
            }
          } else {
            this.thirdStoreVenue.push(element);
          }
        }
      }

    } else {

    }
  }
  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {

    this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,
      id_inventory, quantity, id_state_inv_detail,
      id_product, id_category, stock,
      stock_min, img_url, id_tax,
      id_common_product, name_product,
      description_product, id_state_product,
      id_product_third, location,
      id_third, id_category_third,

      id_state_prod_third, state_prod_third,
      id_measure_unit, id_measure_unit_father,
      id_common_measure_unit, name_measure_unit,
      description_measure_unit, id_state_measure_unit,
      state_measure_unit, id_code,
      code, img,
      id_state_cod, state_cod)


      .subscribe((data: InventoryDetail[]) => this.inventoryList = data,
      error => console.log(error),
      () => {
        if (this.inventoryList.length > 0) {

        }
      });

  }

  beginPlusOrDiscount(id_inventory, inventoryQuantityDTOList, typeExit?: boolean) {
    console.log("enviar... ", this.inventoryQuantityDTOList);
    if (typeExit) {
      this.inventoriesService.putDiscountInventory(id_inventory, this.inventoryQuantityDTOList)
        .subscribe(result => {
          if (result) {
            console.log("CORRECT")
            this.itemLoadBilling = null;
            this.detailBillingDTOList = [];
            this.isCode = false;
            this.isDetail = false;
            this.form.reset();
            this.getInventoryList(this.STATE, null, null, null, null, null, this.CURRENT_ID_THIRD_PATHER)
            this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info')

          } else {
            console.log("INCORRECT")

          }
        })

    } else {
      this.inventoriesService.putPlusInventory(id_inventory, this.inventoryQuantityDTOList)
        .subscribe(result => {
          if (result) {
            console.log("CORRECT")
            this.itemLoadBilling = null;
            this.detailBillingDTOList = [];
            this.isDetail = false;
            this.isCode = false;
            this.form.reset();
            this.getInventoryList(this.STATE, null, null, null, null, null, this.CURRENT_ID_THIRD_PATHER)
            this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ", 'info')

          } else {
            console.log("INCORRECT")

          }
        })
    }
  }

}
