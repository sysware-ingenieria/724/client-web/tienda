import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { BillComponent } from './bill/bill.component';
import { BillDataComponent } from './bill-data/bill-data.component';
import { BillGeneralComponent } from './bill-general/bill-general.component';

// new
import { BillInputComponent } from './bill-input/bill-input.component';
import { BillOutputComponent } from './bill-output/bill-output.component';
import { BillSaleComponent } from './bill-sale/bill-sale.component';
import { BillRemissionComponent } from './bill-remission/bill-remission.component';

import { BillShopComponent } from './bill-shop/bill-shop.component';
import { BillReturnComponent } from './bill-return/bill-return.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const BillingRouting: Routes = [
  
    { path: 'billing', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: BillDataComponent},
        { path: 'general', component: BillGeneralComponent},
        { path: 'begin', component: BillComponent },
        { path: 'sale', component: BillSaleComponent },
        { path: 'shop', component: BillShopComponent },
        { path: 'input', component: BillInputComponent },
        { path: 'output', component: BillOutputComponent },
        { path: 'remission', component: BillRemissionComponent },//translados
        { path: 'return', component: BillReturnComponent },
        
        
         
  
      ]
    }
  
  ]