
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { FormGroup, FormArray,FormBuilder, Validators } from '@angular/forms';


import {MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { DocumentService } from '../../document/document.service';
import { BillingService } from '../billing.service';

import { BarCodeService } from '../../../store724/bar-codes/bar-code.service';
import { AttributeService } from '../../../store724/attributes/attribute.service';


import { AttributeDetailList} from '../../../store724/attributes/models/attributeDetailList'
import { AttributeValue } from '../../../store724/attributes/models/attributeValue'
import { Third } from '../../../thirds724/third/models/third';


import { Code } from '../../../store724/bar-codes/models/code';


import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';
import { DialogQuantityBillComponent } from '../dialog-quantity-bill/dialog-quantity-bill.component'
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'

var inventoryList:InventoryDetail[];
var inventoryListGlobal:InventoryDetail[];


import { FilterInventory } from '../../../store724/inventories/models/filters'
import { Bill } from '../models/bill';
import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';
import { CommonStateDTO } from '../../commons/commonStateDTO';
import { DocumentDTO } from '../../commons/documentDTO';


@Component({
  selector: 'app-bill-data',
  templateUrl: './bill-data.component.html',
  styleUrls: ['./bill-data.component.scss']
})
export class BillDataComponent implements OnInit {


  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER=0;
  isExit=true;
  form: FormGroup;
  

  thirdFathet:Third;
  inventoryListAux:Inventory[];
  attributeDetailList:AttributeDetailList[]
  selectValues:AttributeValue[];   
  attributeValueList:AttributeValue[];
  inventoryQuantityDTOList:InventoryQuantityDTO[];    
  billDTO:BillDTO;
  detailBillDTO:DetailBillDTO;
  detailBillDTOList:DetailBillDTO[];
  commonStateDTO:CommonStateDTO;
  documentDTO:DocumentDTO;
  
  detailBillTemp:any[];
  
  thirdAux:Inventory[];

  codeList:Code[];
  id_codeList:number[];

  dataBilling:InventoryDetail;


  displayedColumns = ['position', 'name', 'weight', 'location', 'symbol', 'direccion'];
  displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
  dataSource:InventoryDataSource;

  filterInventory:FilterInventory;
  



  constructor(public locStorage: LocalStorage, private fb: FormBuilder,
    private _router: Router,public billingService:BillingService,
    public barCodeService:BarCodeService,public documentService:DocumentService,
    public attributeService:AttributeService,
    public inventoriesService: InventoriesService, public dialog: MatDialog) { 
      inventoryList=[]
      this.selectValues
      this.attributeValueList=[]
      this.filterInventory= new FilterInventory(null,null,null,null,null,null,null,null,null,null,null,this.attributeValueList);
      this.detailBillTemp = [];
      this.createControls();
      
      this.inventoryQuantityDTOList=[];
      this.commonStateDTO= new CommonStateDTO(null, null, null);
      this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO);
      this.detailBillDTOList=[];
      this.documentDTO = new DocumentDTO(null, null);      
      this.billDTO=new BillDTO(null, null, null,null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList,this.documentDTO);
    }




  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER=this.token.id_third_father;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getInventoryList(this.STATE,null,null,null,null,null,this.CURRENT_ID_THIRD_PATHER)
        
      } 
      

    }

  }

  showFilter(event):void{
    
    this.filterInventory=event;

    let datas
    let ID_INVENTORY=-1
    this.inventoriesService.postInventoriesDetailListFilters(this.CURRENT_ID_THIRD_PATHER,ID_INVENTORY,this.filterInventory)
        .subscribe((data: InventoryDetail[]) => inventoryList = data,
        error => console.log(error),
        () => {
          console.log("RESPUESTA INMEDIATA "+inventoryList)    
          this.dataSource = new InventoryDataSource();
                   // this.getInventoryList(this.STATE,null,null,null,null,null,this.CURRENT_ID_THIRD_PATHER)

        });

    //this.beginFilters()
          
  }

  beginFilters(){

    inventoryList=[]
    this.dataSource = new InventoryDataSource();
        if(this.filterInventory.id_cat_productor!==null){
      
    
            this.getInventoryList(this.STATE,null,null,null,null,null,this.filterInventory.id_venue,
              this.filterInventory.id_category_th,null,null,null,this.filterInventory.id_cat_productor,null,
              null,null,null,null,null,null,null,null,null,this.filterInventory.id_measure_unit,null,null,
              null,null,null,null,null,null,null,null,null,null,null,null);
    
          
        }else{
    
            this.getInventoryList(this.STATE,null,null,null,null,null,this.filterInventory.id_venue,
              this.filterInventory.id_category_th,null,null,null,this.filterInventory.id_productor,null,
              null,null,null,null,null,null,null,null,null,this.filterInventory.id_measure_unit,null,null,
              null,null,null,null,null,null,null,null,null,null,null,null);
        }
  }


  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  getInventoryList(state_inv_detail?:number,state_product?:number,id_inventory_detail?:number, 
    id_inventory?:number, id_product_third?:number,location?:number,
    id_third?:number,id_category_third?:number, quantity?:number,id_state_inv_detail?:number,
    id_product?:number,id_category?:number,stock?:number,
    stock_min?:number,img_url?:string,id_tax?:number,
    id_common_product?:number,name_product?:string,
    description_product?:string,id_state_product?:number,
  

    id_state_prod_third?:number,state_prod_third?:number,
    id_measure_unit?:number,id_measure_unit_father?:number,
    id_common_measure_unit?:number,name_measure_unit?:string,
    description_measure_unit?:string,id_state_measure_unit?:number,
    state_measure_unit?:number,id_code?:number,
    code?:number,img?:string,
    id_attribute_list?:number,
    id_state_cod?:number,state_cod?:number,
    attribute?:number,
    attribute_value?:number){


    if((attribute!==null && attribute>0)  || (attribute_value!==null && attribute_value>0)){
    
      this.loadInventorAttr(state_inv_detail,state_product,id_inventory_detail, 
        id_inventory, id_product_third,location,
        id_third,id_category_third, quantity,id_state_inv_detail,
        id_product,id_category,stock,
        stock_min,img_url,id_tax,
        id_common_product,name_product,
        description_product,id_state_product,
      
    
        id_state_prod_third,state_prod_third,
        id_measure_unit,id_measure_unit_father,
        id_common_measure_unit,name_measure_unit,
        description_measure_unit,id_state_measure_unit,
        state_measure_unit,id_code,
        code,img,
        id_attribute_list,
        id_state_cod,state_cod,
        attribute,
        attribute_value);
    }else{
        this.inventoriesService.getInventoriesDetailList(state_inv_detail,state_product,id_inventory_detail, 
                                                          id_inventory, quantity,id_state_inv_detail,
                                                          id_product,id_category,stock,
                                                          stock_min,img_url,id_tax,
                                                          id_common_product,name_product,
                                                          description_product,id_state_product,
                                                          id_product_third,location,
                                                          id_third,id_category_third,
                                                
                                                          id_state_prod_third,state_prod_third,
                                                          id_measure_unit,id_measure_unit_father,
                                                          id_common_measure_unit,name_measure_unit,
                                                          description_measure_unit,id_state_measure_unit,
                                                          state_measure_unit,id_code,
                                                          code,img,
                                                          id_state_cod,state_cod)


        .subscribe((data: InventoryDetail[]) => inventoryList = data,
        error => console.log(error),
        () => {
    
          this.dataSource = new InventoryDataSource();
        }); 
      } 
    }

    loadInventorAttr(state_inv_detail?:number,state_product?:number,id_inventory_detail?:number, 
    id_inventory?:number, id_product_third?:number,location?:number,
    id_third?:number,id_category_third?:number, quantity?:number,id_state_inv_detail?:number,
    id_product?:number,id_category?:number,stock?:number,
    stock_min?:number,img_url?:string,id_tax?:number,
    id_common_product?:number,name_product?:string,
    description_product?:string,id_state_product?:number,
  

    id_state_prod_third?:number,state_prod_third?:number,
    id_measure_unit?:number,id_measure_unit_father?:number,
    id_common_measure_unit?:number,name_measure_unit?:string,
    description_measure_unit?:string,id_state_measure_unit?:number,
    state_measure_unit?:number,id_code?:number,
    code?:number,img?:string,
    id_attribute_list?:number,
    id_state_cod?:number,state_cod?:number,
    attribute?:number,
    attribute_value?:number){

  
        
      this.attributeService.getAttributeDetailList(null,null,null,null,attribute_value,attribute)
          .subscribe((data: AttributeDetailList[]) => this.attributeDetailList = data,
          error => console.log(error),
            () => {
              inventoryList=[]
              for(let object of this.attributeDetailList){

                alert(object.id_attribute_list,)

                this.inventoriesService.getInventoriesDetailList(state_inv_detail,state_product,id_inventory_detail, 
                  id_inventory, quantity,id_state_inv_detail,
                  id_product,id_category,stock,
                  stock_min,img_url,id_tax,
                  id_common_product,name_product,
                  description_product,id_state_product,
                  id_product_third,location,
                  id_third,id_category_third,
          
                  id_state_prod_third,state_prod_third,
                  id_measure_unit,id_measure_unit_father,
                  id_common_measure_unit,name_measure_unit,
                  description_measure_unit,id_state_measure_unit,
                  state_measure_unit,id_code,
                  code,img,
                  object.id_attribute_list,
                  id_state_cod,state_cod)
                .subscribe((data: InventoryDetail[]) =>  inventoryList.concat(data),
                error => console.log(error),
                () => {
                  console.log("LISTA-> ",inventoryList)

                
                  this.dataSource = new InventoryDataSource();
                });


              }

              this.dataSource = new InventoryDataSource();

            
        
              
        });
        
  }

  iscloseMovement:Boolean=false;
  closeMovement(){
      this.iscloseMovement=!this.iscloseMovement
  }
  changeTypeBill(){
    this.isExit=!this.isExit
    this.iscloseMovement=false
    this.getInventoryList(this.STATE,null,null,null,null,null,this.CURRENT_ID_THIRD_PATHER)
    this.detailBillTemp=[];
}

  changeQuantity(element){

    let oldQuantity=element.detail.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(DialogQuantityBillComponent, {
      height: '450px',
      width: '600px',
      data: { quantityTemp:element,isExit:this.isExit
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    
      //console.log(result);
      if(result){


        console.log("Cantidad Vieja");
        console.log(oldQuantity);
        inventoryDetail=result;
        console.log(inventoryDetail);
        if (this.detailBillTemp.length > 0) {

          console.log(" CASO 1 ");
          for (let i = 0; i < this.detailBillTemp.length; i++) {
            console.log("IF  1 ", this.detailBillTemp[i].detail.detail.id_inventory_detail);
            console.log("IF  2 ", element.detail.id_inventory_detail);
            
            if (this.detailBillTemp[i].detail.detail.id_inventory_detail === element.detail.id_inventory_detail) {
          
              //this.newProductsForMovement[i].detail.detail.quantity = result.detail.quantity + this.newProductsForMovement[i].detail.detail.quantity
              flag = false;
              console.log("FLAG 1", flag);
            }
          }
        }

        console.log("FLAG 2", flag);
        if (flag || this.detailBillTemp.length === 0) {
          console.log("DENTRO FINAL... ", this.detailBillTemp);
          flag = false

          this.detailBillTemp.push({
            "is_exit":this.isExit,
            "oldQuantity":oldQuantity,
            "detail":result
          });
        }

       
        
  
        console.log("listaA Factura: ", this.detailBillTemp );
      }
      
      // if(result)
    });

  }

  deleteDetBill(i){
    let detailInv=this.detailBillTemp[i]
    console.log("DELETE", this.detailBillTemp[i])

    console.log(" DETAIL INV", detailInv)

    for(let i=0; i<inventoryList.length; i++){
      
      if(inventoryList[i].detail.id_inventory_detail==detailInv.detail.detail.id_inventory_detail){
          // si es de agregacion, se debe restar el valor alborrar
          console.log(" DETAIL Issss ",inventoryList[i].detail.quantity)
          if(detailInv.is_exit){
            inventoryList[i].detail.quantity=(inventoryList[i].detail.quantity)+ (detailInv.oldQuantity - detailInv.detail.detail.quantity)

          }else{
            inventoryList[i].detail.quantity=(inventoryList[i].detail.quantity)-(detailInv.detail.detail.quantity - detailInv.oldQuantity)
          }

          console.log(" FIN ",inventoryList[i].detail.quantity)
      }
    }
    this.detailBillTemp.splice(i,1);
    console.log(this.detailBillTemp.length)
    if(this.detailBillTemp.length<=0){
      this.iscloseMovement=false
    }

    this.dataSource = new InventoryDataSource();


    
  }
  
  createControls() {
    this.form = this.fb.group({
      description: ['', Validators.compose([
        Validators.required
      ])]
    });
  }
  inventoryQuantityDTO:InventoryQuantityDTO;

  beginPlusOrDiscount(id_inventory,inventoryQuantityDTOList,typeExit?:boolean){      
        console.log("enviar... ",this.inventoryQuantityDTOList);
        
        if(typeExit){
          this.inventoriesService.putDiscountInventory(id_inventory, this.inventoryQuantityDTOList)
          .subscribe(result=>{
            if(result){
              console.log("CORRECT")
              this.form.reset();
              
              this.iscloseMovement=false
              this.detailBillTemp =[];
            }else{
              console.log("INCORRECT")
              
            }
          })
          
        }else{
          this.inventoriesService.putPlusInventory(id_inventory, this.inventoryQuantityDTOList)
          .subscribe(result=>{
            if(result){
              console.log("CORRECT")
              this.form.reset();
              this.iscloseMovement=false
              
              this.detailBillTemp =[];
            }else{
              console.log("INCORRECT")
              
            }
          })
        }
      }





  saveBilling(is_exit:boolean){
    this.commonStateDTO.state=1
    let id_inventory=null
    this.detailBillDTOList=[]
    this.inventoryQuantityDTOList=[]

    this.documentDTO.title = 'Movimiento de '+(is_exit?'salida':'entrada')
    this.documentDTO.body=this.form.value["description"];
      /**
     * building detailBill and Quantity
     */
    for(let element of this.detailBillTemp){
      this.inventoryQuantityDTO=new InventoryQuantityDTO(null,null,null,null);
      id_inventory=element.detail.detail.id_inventory;

      //building detailBill
      this.detailBillDTO.price=0
      this.detailBillDTO.tax=0
      this.detailBillDTO.id_product_third=element.detail.description.id_product_third
      this.detailBillDTO.tax_product=0
      this.detailBillDTO.state = this.commonStateDTO;

       //building Quantity
      // Inv Quantity
      this.inventoryQuantityDTO.id_inventory_detail= element.detail.detail.id_inventory_detail; 
      this.inventoryQuantityDTO.id_product_third = element.detail.description.id_product_third ;
      this.inventoryQuantityDTO.code = element.detail.detail.code;

      if(element.is_exit){ //De salida
        let discount = element.oldQuantity - element.detail.detail.quantity;
           //Detail billing
        this.detailBillDTO.quantity = discount;

        // Inv Quantity
        this.inventoryQuantityDTO.quantity = discount; 

      }else{ // Entrada
        let discount = (element.detail.detail.quantity - element.oldQuantity);
        //Detail billing
        this.detailBillDTO.quantity= discount;

        // Inv Quantity        
        this.inventoryQuantityDTO.quantity = discount; 
       
      }
      this.inventoryQuantityDTOList.push(
        this.inventoryQuantityDTO
      )
      this.detailBillDTOList.push(this.detailBillDTO);


    }  


    // comprobar lista de details bill, si esta vacia no hacer nada al service
    

    //building master bill
    this.billDTO.id_third_employee=this.token.id_third;
    this.billDTO.id_third = this.token.id_third_father;
    this.billDTO.id_bill_type = 1;
    this.billDTO.id_bill_state= 1;
    
    this.billDTO.purchase_date = new Date();
    this.billDTO.subtotal= 0;
    this.billDTO.tax = 0;
    this.billDTO.totalprice = 0;
    this.billDTO.discount = 0;
    this.billDTO.documentDTO=this.documentDTO;
    this.billDTO.state = this.commonStateDTO;
    this.billDTO.details = this.detailBillDTOList;

    this.billingService.postBillResource(this.billDTO)
    .subscribe(
      result=>{
        if(result){
          this.beginPlusOrDiscount(id_inventory, this.inventoryQuantityDTOList,is_exit)
        }
      }
    )
  }



}



export class InventoryDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<InventoryDetail[]> {

    return Observable.of(inventoryList);
  }

  disconnect() {}
}
