import { Component, OnInit, Inject, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


/*
*    Material modules for component
*/

/*
*     others component
*/

/*
*     models for  your component
*/
import { CommonThird } from '../../../commons/CommonThird'
import { DocumentType } from '../../../thirds724/document-type/models/document-type'

/*
*     services of  your component
*/
import { DocumentTypeService } from '../../../thirds724/document-type/document-type.service';
import { ThirdService } from '../../../thirds724/third/third.service';


/*
*     constant of  your component
*/
import { InventoriesService } from '../../../store724/inventories/inventories.service';


@Component({
  selector: 'app-bill-dialog-third',
  templateUrl: './bill-dialog-third.component.html',
  styleUrls: ['./bill-dialog-third.component.scss']
})
export class BillDialogThirdComponent implements OnInit {


  documentTypes: DocumentType[];
  form: FormGroup;

  // Usamos el decorador Output
  @Output() EmitDocumentType = new EventEmitter();
  public nombre: string;
  public document_type: number;
  public document_number: String;

  checked = false;
  indeterminate = false;
  align = 'start';
  disabled = true;

  thirdSearched : any;

  constructor(
    public dialogRef: MatDialogRef<BillDialogThirdComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
    public inventoriesService: InventoriesService,
    private thirdService: ThirdService, public docTypeService: DocumentTypeService) {
      this.thirdSearched = null;
     }

  ngOnInit() {
    this.getDocumentType();
    this.nombre = "Pueblo de la Toscana";
    this.createControls();

  }

  isChecked() {
    this.disabled = !this.disabled;
  }


  // GET /documents-types
  getDocumentType(): void {

    this.docTypeService.getDocumentTypeList()
      .subscribe((data: DocumentType[]) => this.documentTypes = data,
      error => console.log(error),
      () => {

      });
  }

  filterThird() {
    let response: any[];

    console.log("type doc --> ", this.form.value['document_type']);
    console.log("doc number--> ", this.form.value['document_number']);


    this.thirdService.getThirdList(null, null, +this.form.value['document_type'], this.form.value['document_number'])

      .subscribe((data: any[]) => response = data,
      error => { console.log(error) },
      () => {
        if (response.length > 0) {
          console.log("RESPONSE THIRD TIENDA --> ", response)
          this.thirdSearched = response;

          this.dialogRef.close(this.thirdSearched);
        } else {

          alert("Tercero no encontrado");

          // this.thirdService.getThirdList(null, null, null, null, +this.form.value['document_type'], this.form.value['document_number'])

          //   .subscribe((data: any[]) => response = data,
          //   error => { console.log(error) },
          //   () => {
          //     if (response.length > 0) {
          //       console.log("RESPONSE THIRD PERSONA--> ", response)
          //     } else {
          //       console.log("tercero no encontradOO")
          //     }

          //   })
        }
      })

  }

  createControls() {
    this.form = this.fb.group({
      document_type: ['', Validators.compose([
        Validators.required
      ])],
      document_number: ['', Validators.compose([

      ])],
    });
  }



}
