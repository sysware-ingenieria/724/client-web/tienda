
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';


import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { DialogQuantityBillComponent } from '../dialog-quantity-bill/dialog-quantity-bill.component'


/*
*     Models of  your component
*/
import { Token } from '../../../../../../shared/token';

import { Third } from '../../../thirds724/third/models/third';
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';

import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';
import { CommonStateDTO } from '../../commons/commonStateDTO';
import { DocumentDTO } from '../../commons/documentDTO'
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'

import { InventoryDTO } from '../../../store724/inventories/models/inventoryDTO';
import { InventoryDetailDTO } from '../../../store724/inventories/models/inventoryDetailDTO';
import { CommonStateStoreDTO } from '../../../store724/commons/CommonStateStoreDTO'

import { CommonStoreDTO } from '../../../store724/commons/CommonStoreDTO'


/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage';

import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { DocumentService } from '../../document/document.service';
import { BillingService } from '../billing.service';
import { ThirdService } from '../../../thirds724/third/third.service'
import { BarCodeService } from '../../../store724/bar-codes/bar-code.service';
import { Bill } from 'app/tienda724/dashboard/business/billing724/billing/models/bill';
@Component({
  selector: 'app-bill-general',
  templateUrl: './bill-general.component.html',
  styleUrls: ['./bill-general.component.scss']
})
export class BillGeneralComponent implements OnInit {

  type_billing: string
  isMyProduct = true;
  ID_THIRD_TYPE: number;
  STATE = 1
  inventoryList: InventoryDetail[];
  inventoryDetailForBilling: InventoryDetail[];


  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER = 0;
  ID_THIRD_FATHER = 0;
  isThird = true;

  isExit = true;
  form: FormGroup;
  isVenueThird = true;


  //models
  newProductsForMovement: any[];
  inventories: Inventory[];
  inventorieDTO: InventoryDTO;
  inventoryDetailDTO:InventoryDetailDTO;
  inventoryDetailDTOList:InventoryDetailDTO[];
  

  thirdListList: Third[];
  token: Token;
  third: Third;
  thirdStores: Third[];
  thirdStoreVenue: Third[];

  inventoryQuantityDTO:InventoryQuantityDTO;      
  inventoryQuantityDTOList:InventoryQuantityDTO[];      
  inventoryQuantityDTOListOrigin:InventoryQuantityDTO[];     
  billDTO:BillDTO;
  detailBillDTO:DetailBillDTO;
  detailBillDTOList:DetailBillDTO[];

  commonStateStoreDTO: CommonStateStoreDTO; 
  commonDTO:CommonStoreDTO;
  
  commonStateDTO:CommonStateDTO;
  documentDTO:DocumentDTO;
  


  constructor(public locStorage: LocalStorage, private fb: FormBuilder,
    public thirdService: ThirdService,

    private _router: Router, public billingService: BillingService,
    public barCodeService: BarCodeService, public documentService: DocumentService,
    public inventoriesService: InventoriesService, public dialog: MatDialog) {
    this.type_billing = "TRANSLADO DE MERCANCIA"
    this.thirdStoreVenue = []
    this.inventoryList = []
    this.createControls()
    this.logNameChange()
    this.loadData()

    this.inventoryDetailForBilling = []
    this.newProductsForMovement = []

    this.inventoryQuantityDTOList=[];
    this.inventoryQuantityDTOListOrigin=[]
    this.inventoryQuantityDTO=new InventoryQuantityDTO(null,null,null,null);
    this.commonStateStoreDTO = new CommonStateStoreDTO(1, null, null)
    this.commonDTO= new CommonStoreDTO(null,null);
    
    this.inventorieDTO= new InventoryDTO(null,this.commonDTO,this.commonStateStoreDTO);
    this.inventoryDetailDTO= new InventoryDetailDTO(null,null,null,null,this.commonStateStoreDTO)
    this.inventoryDetailDTOList=[]


    this.commonStateDTO= new CommonStateDTO(null, null, null);
    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO);
    this.detailBillDTOList=[];
    this.documentDTO = new DocumentDTO(null, null);      
    this.billDTO=new BillDTO(null, null, null,null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList,this.documentDTO);
  }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.third = this.locStorage.getThird();
      this.CURRENT_ID_THIRD = this.token.id_third ? this.token.id_third : null;
      this.ID_THIRD_FATHER = this.token.id_third_father ? this.token.id_third_father : null;


      if (this.ID_THIRD_FATHER > 0) {
        this.isThird = false;
      }

      this.getThird(true, this.STATE, null, this.CURRENT_ID_THIRD)
      this.getThird(true, this.STATE, null, this.ID_THIRD_FATHER)
      this.getInventoryID(this.STATE, null, this.ID_THIRD_FATHER)
     

    }
  }
  getInventoryID(state?:number,id_inventory?:number,id_third?:number){
    
      this.inventoriesService.getInventoriesList(state,id_inventory,id_third)
        .subscribe((data: Inventory[]) => this.inventories = data,
        error => console.log(error),
        () => {
          if(this.inventories.length>0){
            let id_inv=this.inventories[0].id_inventory
            if(id_inv>0){
              console.log("tiene invenntario")
              this.getInventoryList(this.STATE, null,null, id_inv, null, null, this.ID_THIRD_FATHER)
                
              
            }
            alert("tiene inventario "+ id_inv)
            

          }else{
            alert("No tiene inventario")
           
          }
        });
}


  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }
  createControls() {
    this.form = this.fb.group({
      id_th_venue: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([
        Validators.required
      ])],
      date: ['', Validators.compose([
        Validators.required
      ])]

    });
  }

  loadData() {
    this.form.patchValue({
      date: new Date()
    });

  }

  deleteDetBill(i) {
    console.log("Sacar de la lista para transladar");
    let detail = this.inventoryDetailForBilling[i]
    for (let i = 0; i < this.inventoryList.length; i++) {

      if (this.inventoryList[i].detail.id_inventory_detail == detail.detail.id_inventory_detail) {

        this.inventoryList[i].detail.quantity = this.inventoryList[i].detail.quantity + detail.detail.quantity

      }

    }
    //this.inventoryDetailForBilling.slice(i,1);

  }

  addDetBill(i, element) {
    console.log("Agregar a la lista nueva");

    this.changeQuantity(element, true)

  }

  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {

    this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,
      id_inventory, quantity, id_state_inv_detail,
      id_product, id_category, stock,
      stock_min, img_url, id_tax,
      id_common_product, name_product,
      description_product, id_state_product,
      id_product_third, location,
      id_third, id_category_third,

      id_state_prod_third, state_prod_third,
      id_measure_unit, id_measure_unit_father,
      id_common_measure_unit, name_measure_unit,
      description_measure_unit, id_state_measure_unit,
      state_measure_unit, id_code,
      code, img,
      id_state_cod, state_cod)


      .subscribe((data: InventoryDetail[]) => this.inventoryList = data,
      error => console.log(error),
      () => {
        if (this.inventoryList.length > 0) {

        }
      });

  }

  logNameChange() {
    const id_th_venueControl = this.form.get('id_th_venue');

    id_th_venueControl.valueChanges.forEach((value: string) => {

      let id_th_venue: boolean = id_th_venueControl['_value'];

      if (id_th_venue) { // IF Active all
        console.log(" SEDE CMABIO ", id_th_venue);

      }
    }
    );

  }


  changeQuantity(element, isExit: boolean) {

    let oldQuantity = element.detail.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(DialogQuantityBillComponent, {
      height: '450px',
      width: '600px',
      data: {
        quantityTemp: element, isExit: isExit, currentList: this.inventoryDetailForBilling
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      //console.log(result);
      if (result) {

        //this.inventoryDetailForBilling.push(element);

        console.log("RESULT... ", this.newProductsForMovement.length);
 

        if (this.newProductsForMovement.length > 0) {
          console.log(" CASO 1 ");
          for (let i = 0; i < this.newProductsForMovement.length; i++) {
            console.log("IF  1 ", this.newProductsForMovement[i].detail.detail.id_inventory_detail);
            console.log("IF  2 ", element.detail.id_inventory_detail);
            
            if (this.newProductsForMovement[i].detail.detail.id_inventory_detail === element.detail.id_inventory_detail) {
          
              //this.newProductsForMovement[i].detail.detail.quantity = result.detail.quantity + this.newProductsForMovement[i].detail.detail.quantity
              flag = false;
              console.log("FLAG 1", flag);
            }
          }
        }
        console.log("FLAG 2", flag);
        if (flag || this.newProductsForMovement.length === 0) {
          console.log("DENTRO FINAL... ", this.newProductsForMovement);
          flag = false
          this.newProductsForMovement.push(
            {
              "detail": result,
              "oldQuantity": oldQuantity
            }
          )
        }





        console.log("Cantidad Vieja");
        console.log(oldQuantity);
        inventoryDetail = result;
        console.log(inventoryDetail);

        console.log("ARRAY ", this.newProductsForMovement);



      }

      // if(result)
    });

  }

  test(element, quantity, result) {
    if (this.inventoryDetailForBilling.length > 0) {

    } else {
      element.detail.quantity = 1;
      this.inventoryDetailForBilling.push(element)
    }

  }

  // GET /Thirds
  getThird(is_venue: boolean, state?: number, id_third?: number, id_third_father?: number, document_type?: number, document_number?: string, id_doctype_person?: number, doc_person?: string, id_third_type?: number, id_person?: number): void {

    this.thirdService.getThirdList(id_third, id_third_father, document_type, document_number,
      id_doctype_person, doc_person, id_third_type, state, id_person)
      .subscribe((data: Third[]) => this.thirdListList = data,
      error => console.log(error),
      () => {
        
        this.prosessingDataThird(is_venue);
      });
  }

  prosessingDataThird(is_venue: boolean) {

    if (is_venue) {


      for (let element of this.thirdListList) {


        if ((element.profile === null || element.profile === 0) && (element.id_third_father === this.CURRENT_ID_THIRD || element.id_third_father === this.ID_THIRD_FATHER)) {
          if (this.thirdStoreVenue.length > 0) {
            for (let objet of this.thirdStoreVenue) {

              if (element.id_third !== objet.id_third) {

                this.thirdStoreVenue.push(element);
              }
            }
          } else {
            this.thirdStoreVenue.push(element);
          }
        }
      }
      if (this.thirdStoreVenue.length > 0) {
        this.isVenueThird = true;
      } else {
        this.isVenueThird = false;
      }
    } else {

    }
  }

  sentMovement() {

    console.log("INICIA PROCESO DE ENVIO");
    this.commonStateDTO.state=1
    let id_inventory=null
    this.detailBillDTOList=[]
    this.inventoryQuantityDTOList=[]
    let case_1=true;// origen
    let case_2=true;// Destino
    let id_third_venue=null

       /**
     * building detailBill and Quantity
     */
    for(let element of this.newProductsForMovement){
      this.inventoryQuantityDTO=new InventoryQuantityDTO(null,null,null,null);
      id_inventory=element.detail.detail.id_inventory;

        //building detailBill
        this.detailBillDTO.price=0
        this.detailBillDTO.tax=0
        this.detailBillDTO.id_product_third=element.detail.description.id_product_third
        this.detailBillDTO.tax_product=0
        this.detailBillDTO.state = this.commonStateDTO;

         //building Quantity for discount tienda
        // Inv Quantity
    

        if(case_1){ // for discount tienda
         
         
          
          let discount = element.oldQuantity - element.detail.detail.quantity;
          //Detail billing
          this.detailBillDTO.quantity = discount;

           // Inv Quantity for discount tienda
          this.inventoryQuantityDTO.quantity = discount;

          this.inventoryQuantityDTO.id_inventory_detail= element.detail.detail.id_inventory_detail; 
          this.inventoryQuantityDTO.id_product_third = element.detail.description.id_product_third ;
          this.inventoryQuantityDTO.code = element.detail.detail.code;

          this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
          )
  

        }

        if(case_2){ // for plus Quantity
          
         
          console.log("SEDE -> ",this.form.value["id_th_venue"]);
          let discount = element.oldQuantity - element.detail.detail.quantity;
          
            // Inv Quantity for discount tienda
            this.inventoryQuantityDTO.quantity = discount;

          this.inventoryQuantityDTO.id_inventory_detail= null; 
          this.inventoryQuantityDTO.id_product_third = element.detail.description.id_product_third ;
          this.inventoryQuantityDTO.code = element.detail.detail.code;

          this.inventoryQuantityDTOListOrigin.push(
            this.inventoryQuantityDTO
          )
          

          
        
        }

       
       
        this.detailBillDTOList.push(this.detailBillDTO);

    
    }

      // comprobar lista de details bill, si esta vacia no hacer nada al service
      //building master bill
      if(case_1){ // for plus Quantity
        this.documentDTO.title = 'Translado de salida'
        this.documentDTO.body=this.form.value["description"];

        this.billDTO.id_third_employee=this.token.id_third;
        this.billDTO.id_third = this.token.id_third_father;
        this.billDTO.id_bill_type = 1;
        this.billDTO.id_bill_state= 1;
        
        this.billDTO.purchase_date = this.form.value['date'];
        this.billDTO.subtotal= 0;
        this.billDTO.tax = 0;
        this.billDTO.totalprice = 0;
        this.billDTO.discount = 0;
        this.billDTO.documentDTO=this.documentDTO;
        this.billDTO.state = this.commonStateDTO;
        this.billDTO.details = this.detailBillDTOList;
    
        // this.billingService.postBillResource(this.billDTO)
        // .subscribe(
        //   result=>{
        //     if(result){
        //       this.beginPlusOrDiscount(id_inventory, this.inventoryQuantityDTOList,true,id_third_venue)
        //     }
        //   }
        // )
      
      
      }

      if(case_2){ // for plus Quantity
        this.documentDTO.title = 'Translado de Entrada'
        this.documentDTO.body=this.form.value["description"];

        id_third_venue=this.form.value["id_th_venue"];

        this.billDTO.id_third_employee=this.token.id_third;
        this.billDTO.id_third = this.token.id_third_father;
        this.billDTO.id_bill_type = 1;
        this.billDTO.id_bill_state= 1;
        
        this.billDTO.purchase_date = this.form.value['date'];
        this.billDTO.subtotal= 0;
        this.billDTO.tax = 0;
        this.billDTO.totalprice = 0;
        this.billDTO.discount = 0;
        this.billDTO.documentDTO=this.documentDTO;
        this.billDTO.state = this.commonStateDTO;
        this.billDTO.details = this.detailBillDTOList;

        //mismo proceso  de mis productos

        this.getInventoryVenue(this.STATE,null,id_third_venue,this.billDTO,this.inventoryQuantityDTOListOrigin);
    
        this.billingService.postBillResource(this.billDTO)
        .subscribe(
          result=>{
            if(result){
              this.beginPlusOrDiscount(id_inventory, this.inventoryQuantityDTOListOrigin,true,id_third_venue)
            }
          }
        )
      
      
      }
   


  }

  beginPlusOrDiscount(id_inventory,inventoryQuantityDTOList,typeExit?:boolean, id_third_venue?:number){      
    console.log("enviar... ",this.inventoryQuantityDTOList);
    
    if(typeExit){
      this.inventoriesService.putDiscountInventory(id_inventory, this.inventoryQuantityDTOList)
      .subscribe(result=>{
        if(result){
          console.log("CORRECT")
          this.form.reset();
          this.newProductsForMovement =[];
        }else{
          console.log("INCORRECT")
          
        }
      })
      
    }else{
      this.inventoriesService.putPlusInventory(id_inventory, this.inventoryQuantityDTOList)
      .subscribe(result=>{
        if(result){
          console.log("CORRECT")
          this.form.reset();
        }else{
          console.log("INCORRECT")
          
        }
      })
    }
  }

  getInventoryVenue(state?:number,id_inventory?:number,id_third?:number, billDTO?:BillDTO,inventoryQuantityDTOList?:InventoryQuantityDTO[]){
    
      this.inventoriesService.getInventoriesList(state,id_inventory,id_third)
        .subscribe((data: Inventory[]) => this.inventories = data,
        error => console.log(error),
        () => {
          if(this.inventories.length>0){
            let id_inv=this.inventories[0].id_inventory
            if(id_inv>0){
              console.log("tiene invenntario")

                 this.billingService.postBillResource(this.billDTO)
                  .subscribe(
                    result=>{
                      if(result){
                        this.beginPlusOrDiscount(id_inv, this.inventoryQuantityDTOListOrigin,false)
                      }
                    }
                  )
              
            }
            alert("tiene inventario "+ id_inv)
            

          }else{
            alert("No tiene inventario")
            this.createInventory(id_third, billDTO,inventoryQuantityDTOList)
          }
        });
      }

      createInventory(id_third?:number, billDTO?:BillDTO,inventoryQuantityDTOList?:InventoryQuantityDTO[]){
        
                this.commonDTO.name="Inventario 1"
                this.commonDTO.name="Inventario para contabilidad de su establecimiento"
                this.commonStateStoreDTO.state=1
        
                this.inventorieDTO.id_third=id_third
                this.inventorieDTO.common=this.commonDTO
                this.inventorieDTO.state=this.commonStateStoreDTO

                for(let element of billDTO.details){

                  this.inventoryDetailDTO.quantity=element.quantity
                  this.inventoryDetailDTO.id_product_third=element.id_product_third
                  this.inventoryDetailDTO.state=this.commonStateStoreDTO
                  this.inventoryDetailDTOList.push(this.inventoryDetailDTO)

                }
        
                
                let id_inventory=null
                this.inventorieDTO.details=this.inventoryDetailDTOList
        
        
        
                this.inventoriesService.postInventory(this.inventorieDTO)
                .subscribe(
                result => {
          
                  if (result >0) {
                    id_inventory=result
                    alert("Su  NUEVO inventario ES "+ id_inventory)

                    this.billingService.postBillResource(this.billDTO)
                    .subscribe(
                      result=>{
                        if(result){
                          this.form.reset();
                          this.newProductsForMovement =[];
                          alert("FACTURA REGISTRADA")
                        }
                       }
                     )
                    
                    
        
                  }
                })
              }


}
