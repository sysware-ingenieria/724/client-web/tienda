import { Component, OnChanges, SimpleChanges, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';


import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


import { BillDialogThirdComponent } from '../bill-dialog-third/bill-dialog-third.component'


@Component({
  selector: 'app-bill-third',
  templateUrl: './bill-third.component.html',
  styleUrls: ['./bill-third.component.scss']
})
export class BillThirdComponent implements OnInit {

  thirdSearched : any;


  constructor(public dialog: MatDialog) { 
    this.thirdSearched = null;
  }

  ngOnInit() {
  }

  searchThird() {
    // let oldQuantity = element.quantity;
    // let inventoryDetail = null;
    // let flag = true;

    let dialogRef = this.dialog.open(BillDialogThirdComponent, {
      height: '450px',
      width: '600px',
      data: {
        // quantityTemp: element, type_name:this.TYPE_NAME, is_exit: element.is_exit,  
        // currentList: this.inventoryDetailForBilling
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        //this.sentNewChangeDetailBill();
        this.thirdSearched = result[0];
        console.log("result popup Docu Type ", this.thirdSearched)
      }
    });

  }
    
    
  }


