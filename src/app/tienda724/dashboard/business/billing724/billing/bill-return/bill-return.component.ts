import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// materials
import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*
*     others component
*/
import { BillDocumentComponent } from '../bill-document/bill-document.component'


/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';
// DTO
import { CommonStateDTO } from '../../commons/commonStateDTO'
import { DocumentDTO } from '../../commons/documentDTO'
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'
import { Bill } from '../models/bill';
import { BillComplete } from '../models/billComplete';
import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';


/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { BillingService } from '../billing.service';
import { error } from 'util';
import { ThirdService } from '../../../thirds724/third/third.service';



/*
*     constant of  your component
*/
declare var $: any;

@Component({
  selector: 'app-bill-return',
  templateUrl: './bill-return.component.html',
  styleUrls: ['./bill-return.component.scss']
})
export class BillReturnComponent implements OnInit {
  //flags
  showBill = false;
  // attribute
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER = 0;
  ID_INVENTORY_TEMP = 0;
  STATE = 1
  ID_BILL_TYPE = 0;
  TOTAL_PRICE = 0;

  TYPE_NAME: string;

  // models
  token: Token;
  form: FormGroup;
  bill: Bill;
  billDescription: any[];
  billComplete: BillComplete;
  person: any;

  //list
  itemLoadBilling: InventoryDetail;
  inventoryList: InventoryDetail[];

  productsBill: any[];
  idFatherEmployee = 0;
  thirdEmployee: any;



  // DTO's
  commonStateDTO: CommonStateDTO;
  detailBillingDTOList: any[];
  inventoryQuantityDTOList: InventoryQuantityDTO[];
  inventoryQuantityDTO: InventoryQuantityDTO;
  detailBillDTO: DetailBillDTO;
  detailBillDTOList: DetailBillDTO[];
  billDTO: BillDTO;
  documentDTO: DocumentDTO;





  constructor(public locStorage: LocalStorage, private _router: Router,
    private fb: FormBuilder, private billingService: BillingService,
    public inventoriesService: InventoriesService,
    private route: ActivatedRoute, private thirdService: ThirdService,public dialog: MatDialog) {
    this.detailBillingDTOList = [];
    this.inventoryQuantityDTOList = [];
    this.inventoryList = []
    this.billDescription = [];
    this.productsBill = [];
    this.createControls();
    this.logNameChange();
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null);
    this.commonStateDTO = new CommonStateDTO(1, null, null);
    this.detailBillingDTOList = []
    this.documentDTO = new DocumentDTO(null, null);
    this.detailBillDTOList = [];


    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO);
    this.billDTO = new BillDTO(null, null, null, null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList, this.documentDTO);



  }

  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.person = this.locStorage.getPerson();
      this.CURRENT_ID_THIRD = this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER = this.token.id_third_father;

      if (this.CURRENT_ID_THIRD !== null && this.CURRENT_ID_THIRD > 0) {

        this.route.queryParams
          .subscribe(params => {
            // get id _bill_type
            this.ID_BILL_TYPE = +params['type'] || 0;
          });

        //this.getInventoryList(this.STATE, null, null, null, null, null, this.CURRENT_ID_THIRD_PATHER)
      }
    }

  }


  //There is to receive DETAILS
  getDetails(event) {
    this.detailBillingDTOList = event;
  }


  showType(event) {

    this.TYPE_NAME = event.type;
    this.ID_INVENTORY_TEMP = event.inventory;
    //this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)


  }

  //The user auth is not valid 
  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  // start controls
  createControls() {
    this.form = this.fb.group({
      consecutive_number: ['', Validators.compose([
        Validators.required
      ])],
      codeProd: ['', Validators.compose([
        Validators.required
      ])],
      title: ['', Validators.compose([
        Validators.required
      ])],
      body: ['', Validators.compose([
        Validators.required
      ])],
    });
  }

  loadData() {
    this.form.patchValue({
      codeProd: ''
    });
  }

  logNameChange() {
    const codeProdControl = this.form.get('codeProd');
    codeProdControl.valueChanges.forEach((value: string) => {


      this.itemLoadBilling = codeProdControl['value'];

      if (this.itemLoadBilling) { // IF Active all
        // call function  loadDatailBillin()
        this.loadDatailBilling(this.itemLoadBilling)

      }
    });



    const consecutive_numberControl = this.form.get('consecutive_number');
    consecutive_numberControl.valueChanges.forEach((value: string) => {
      if (value) {
      }
    });
  }

  loadDatailBilling(element) {
    let flag = false;
    //element.detail.quantity=1;
    if (element) {
      if (element.inv.detail.quantity > 0) {

        if (this.detailBillingDTOList.length > 0) {

          for (let i = 0; i < this.detailBillingDTOList.length; i++) {

            if (this.detailBillingDTOList[i].item.detail.id_inventory_detail === element.inv.detail.id_inventory_detail) {
              flag = true;
            }
          }

          if (!flag) {
            this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.inv.description.standard_price)
            this.detailBillingDTOList.push(
              { 'is_exit': 0, 'item': element.inv, 'OldQuantity': element.bill.quantity, 'quantity': 1 }
            );
          }

        } else {
          this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.inv.description.standard_price)
          this.detailBillingDTOList.push(
            { 'is_exit': 0, 'item': element.inv, 'OldQuantity': element.bill.quantity, 'quantity': 1 }
          );
        }

      } else {
        this.showNotification('top', 'center', 3, "<h3>El Producto esta agotado</h3> ", 'warning')
      }


      this.loadData();


    }
  }// end controls

  buildProductThirdList() {

  }
  // The method can do bill's search by consecutive number, So You can get the detail with the products.
  // we need only id_product_third and quantity
  searchBill() {
    this.billDescription = [];

    this.getBilling(this.STATE, this.form.value['consecutive_number']);

  }

  // Notifications
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  // services
  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {

    this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,
      id_inventory, quantity, id_state_inv_detail,
      id_product, id_category, stock,
      stock_min, img_url, id_tax,
      id_common_product, name_product,
      description_product, id_state_product,
      id_product_third, location,
      id_third, id_category_third,

      id_state_prod_third, state_prod_third,
      id_measure_unit, id_measure_unit_father,
      id_common_measure_unit, name_measure_unit,
      description_measure_unit, id_state_measure_unit,
      state_measure_unit, id_code,
      code, img,
      id_state_cod, state_cod)


      .subscribe((data: InventoryDetail[]) => this.inventoryList = data,
      error => console.log(error),
      () => {
        if (this.inventoryList.length > 0) {
          this.productsBill = [];
          if (this.billDescription[0] !== null) {
            for (let productL of this.inventoryList) {
              for (let productB of this.billDescription[0]['details']) {
                if (productL.detail.id_product_third === productB.id_product_third) {
                  this.productsBill.push(
                    { 'inv': productL, 'bill': productB }
                  );

                }
              }

            }

          }

        }
      });
  }

  getBilling(state?, consecutive?) {

    let response: any[];
    this.billingService.getBillResource(state, null, null, null, null, null, null, null, consecutive)
      .subscribe((data: any[]) => response = data,
      error => {
        console.log(error);
      },
      () => {
        if (response.length > 0) {

          this.showBill = true;

          this.billDescription = response;

          if (this.billDescription[0].details.length > 0) {

            this.getEmployedBill(this.billDescription[0].billing.id_third_employee);


          }

        } else {
          this.showBill = true;
          this.form.patchValue({
            consecutive_number: ''
          });
        }
      }
      );
  }

  getEmployedBill(idEmployee) {
    let response: any[];
    if (idEmployee !== this.CURRENT_ID_THIRD) {
      this.thirdService.getThirdList(idEmployee)

        .subscribe((data: any[]) => response = data,
        error => { console.log(error) },
        () => {
          if (response.length > 0) {
            this.thirdEmployee = response[0].profile;

            this.idFatherEmployee = response[0].id_third_father;
            this.getIdInventory(this.idFatherEmployee);


          }

        })

    } else {
      this.thirdEmployee = this.person
      this.idFatherEmployee = this.CURRENT_ID_THIRD_PATHER;
      this.getIdInventory(this.idFatherEmployee);
    }
  }

  id_inventoryL = 0;
  getIdInventory(idFather) {

    let response: any[];
    this.inventoriesService.getInventoriesList(this.STATE, null, idFather)

      .subscribe((data: any[]) => response = data,
      error => { console.log(error) },
      () => {
        if (response.length > 0) {
          this.id_inventoryL = response[0].id_inventory;

          this.getInventoryList(null, null, null, this.id_inventoryL, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null)

        }

      })

  }


  // end process
  save() {
    if(this.documentDTO.body==null){
      this.showNotification('top', 'center', 2, '<h2> Se requiere el documento</h2>', 'warning')
      return
    }

    for (let objQuantity of this.detailBillingDTOList) {
      this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null);

      this.inventoryQuantityDTO.id_inventory_detail = objQuantity.item.detail.id_inventory_detail;
      this.inventoryQuantityDTO.id_product_third = objQuantity.item.detail.id_product_third;
      this.inventoryQuantityDTO.quantity = objQuantity.quantity;

      this.inventoryQuantityDTOList.push(this.inventoryQuantityDTO)


      //building detailBill
      this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO);

      this.detailBillDTO.price = 0
      this.detailBillDTO.tax = 0
      this.detailBillDTO.id_product_third = objQuantity.item.detail.id_product_third;
      this.detailBillDTO.tax_product = 0
      this.detailBillDTO.state = this.commonStateDTO;

      this.detailBillDTO.quantity = objQuantity.quantity;
      this.detailBillDTOList.push(this.detailBillDTO);


    }


    let response: any[];
    /*  if (this.id_inventoryL !== null) {
       this.inventoriesService.putPlusInventory(+this.id_inventoryL, this.inventoryQuantityDTOList)
 
         .subscribe((data: any[]) => response = data,
         error => { console.log(error) },
         () => {
           if (response) {
             console.log("Response: agregar cantidad ", response)
             this.showNotification('top', 'center', 2, 'Movimiento satisfactorio', 'info')
 
           } else {
             console.log("Error en el servidor al actualizar la cantidad")
           }
 
         })
 
 
       //crear la factura
 
     } */


    if (this.detailBillDTOList.length > 0) {

      this.billDTO.id_third_employee = this.token.id_third;
      this.billDTO.id_third = this.token.id_third_father;
      this.billDTO.id_bill_type = this.ID_BILL_TYPE;
      this.billDTO.id_bill_state = 1;
      this.billDTO.purchase_date = new Date();
      this.billDTO.subtotal = 0;
      this.billDTO.tax = 0;
      this.billDTO.totalprice = 0;
      this.billDTO.discount = 0;
      this.billDTO.documentDTO = this.documentDTO;
      this.billDTO.state = this.commonStateDTO;

      this.billDTO.details = this.detailBillDTOList;
      this.billingService.postBillResource(this.billDTO)
        .subscribe(
        result => {
          if (result) {
            // alert(this.ID_INVENTORY_TEMP)


            this.inventoriesService.putPlusInventory(+this.id_inventoryL, this.inventoryQuantityDTOList)

              .subscribe((data: any[]) => response = data,
              error => { console.log(error) },
              () => {
                if (response) {
                  console.log("Response: agregar cantidad ", response)
                  this.showNotification('top', 'center', 2, 'Movimiento satisfactorio', 'info')

                } else {
                  console.log("Error en el servidor al actualizar la cantidad")
                }

              })
          }
        });
    }
  }
  addDocument(element) {
    let oldQuantity = element.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(BillDocumentComponent, {
      height: '450px',
      width: '600px',
      data: {
        type_name:this.TYPE_NAME,   
        doc: this.documentDTO
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        console.log('DOC ->> ',this.documentDTO);
        console.log('RESULT ->> ',result);
      }
    });

  }
}
