import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
  /** Files for auth process  */
  import { Urlbase } from '../../../../../shared/urls';
  import { LocalStorage } from '../../../../../shared/localStorage';
/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { Bill } from './models/bill'
import { BillComplete } from './models/billComplete'
import { DetailBill } from './models/detailBill'


/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillDTO } from './models/billDTO'
import { BillCompleteDTO } from './models/billCompleteDTO'
import { DetailBillDTO } from './models/detailBillDTO'
import { DetailBillIdDTO } from './models/detailBillIdDTO'
import { BillDetailIDDTO } from './models/billDetailIdDTO'


@Injectable()
export class BillingService {

  api_uri_bill = Urlbase[3] + '/billing';
  api_uri_bill_detail = Urlbase[3] + '/billing-details';

  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }
  public getBillResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
                                id_third_employee?:number,
                                id_third?:number,
                                id_payment_state?:number,
                                id_bill_state?:number,
                                id_bill_type?:number,
                                consecutive?:string,
                                purchase_date?:Date,
                                subtotal?:number,
                                tax?:number,
                                discount?:number,
                                totalprice?:number,
                                id_state_bill?:number,
                                
                                creation_bill?:Date,
                                update_bill?:Date ): Observable<{} | any[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_bill', id_bill ? "" + id_bill : null);
    params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
    params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
    params.set('id_third', id_third ? "" + id_third : null);
    params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
    params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
    params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
    params.set('consecutive', consecutive ? "" + consecutive : null);
    params.set('purchase_date', purchase_date ? "" + purchase_date : null);
    params.set('subtotal', subtotal ? "" + subtotal : null);
    params.set('tax', tax ? "" + tax : null);
    params.set('discount', discount ? "" + discount : null);
    params.set('totalprice', totalprice ? "" + totalprice : null);
    params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
    params.set('state_bill', state_bill ? "" + state_bill : null);
    params.set('creation_bill', creation_bill ? "" + creation_bill : null);
    params.set('update_bill', update_bill ? "" + update_bill : null);
   

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_bill+"/details", this.options)
      .map((response: Response) => <any[]>response.json())
      .catch(this.handleError);
  }



  public getBillDetailsResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
                                    id_third_employee?:number,
                                    id_third?:number,
                                    id_payment_state?:number,
                                    id_bill_state?:number,
                                    id_bill_type?:number,
                                    consecutive?:string,
                                    purchase_date?:Date,
                                    subtotal?:number,
                                    tax?:number,
                                    discount?:number,
                                    totalprice?:number,
                                    id_state_bill?:number,
                                    
                                    creation_bill?:Date,
                                    update_bill?:Date ): Observable<{} | Bill[]> => {

        let params: URLSearchParams = new URLSearchParams();
        params.set('id_bill', id_bill ? "" + id_bill : null);
        params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
        params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
        params.set('id_third', id_third ? "" + id_third : null);
        params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
        params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
        params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
        params.set('consecutive', consecutive ? "" + consecutive : null);
        params.set('purchase_date', purchase_date ? "" + purchase_date : null);
        params.set('subtotal', subtotal ? "" + subtotal : null);
        params.set('tax', tax ? "" + tax : null);
        params.set('discount', discount ? "" + discount : null);
        params.set('totalprice', totalprice ? "" + totalprice : null);
        params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
        params.set('state_bill', state_bill ? "" + state_bill : null);
        params.set('creation_bill', creation_bill ? "" + creation_bill : null);
        params.set('update_bill', update_bill ? "" + update_bill : null);


        let myOption: RequestOptions = this.options;
        myOption.search = params;
        return this.http.get(this.api_uri_bill, this.options)
        .map((response: Response) => <Bill[]>response.json())
        .catch(this.handleError);
      }

      public getBillCompleteResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
        id_third_employee?:number,
        id_third?:number,
        id_payment_state?:number,
        id_bill_state?:number,
        id_bill_type?:number,
        consecutive?:string,
        purchase_date?:Date,
        subtotal?:number,
        tax?:number,
        discount?:number,
        totalprice?:number,
        id_state_bill?:number,
        
        creation_bill?:Date,
        update_bill?:Date ): Observable<{} | BillComplete[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_bill', id_bill ? "" + id_bill : null);
      params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
      params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
      params.set('id_third', id_third ? "" + id_third : null);
      params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
      params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
      params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
      params.set('consecutive', consecutive ? "" + consecutive : null);
      params.set('purchase_date', purchase_date ? "" + purchase_date : null);
      params.set('subtotal', subtotal ? "" + subtotal : null);
      params.set('tax', tax ? "" + tax : null);
      params.set('discount', discount ? "" + discount : null);
      params.set('totalprice', totalprice ? "" + totalprice : null);
      params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
      params.set('state_bill', state_bill ? "" + state_bill : null);
      params.set('creation_bill', creation_bill ? "" + creation_bill : null);
      params.set('update_bill', update_bill ? "" + update_bill : null);
      
      
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri_bill+"/completes", this.options)
      .map((response: Response) => <BillComplete[]>response.json())
      .catch(this.handleError);
}
  
public getBillCompleteDeatilsResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
      id_third_employee?:number,
      id_third?:number,
      id_payment_state?:number,
      id_bill_state?:number,
      id_bill_type?:number,
      consecutive?:string,
      purchase_date?:Date,
      subtotal?:number,
      tax?:number,
      discount?:number,
      totalprice?:number,
      id_state_bill?:number,
      
      creation_bill?:Date,
      update_bill?:Date ): Observable<{} | any[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_bill', id_bill ? "" + id_bill : null);
      params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
      params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
      params.set('id_third', id_third ? "" + id_third : null);
      params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
      params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
      params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
      params.set('consecutive', consecutive ? "" + consecutive : null);
      params.set('purchase_date', purchase_date ? "" + purchase_date : null);
      params.set('subtotal', subtotal ? "" + subtotal : null);
      params.set('tax', tax ? "" + tax : null);
      params.set('discount', discount ? "" + discount : null);
      params.set('totalprice', totalprice ? "" + totalprice : null);
      params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
      params.set('state_bill', state_bill ? "" + state_bill : null);
      params.set('creation_bill', creation_bill ? "" + creation_bill : null);
      params.set('update_bill', update_bill ? "" + update_bill : null);
      
      
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri_bill+"/completes/details", this.options)
      .map((response: Response) => <any[]>response.json())
      .catch(this.handleError);
      }

      public postBillResource = (body:BillDTO): Observable<number[] | any> => {
        
        let params: URLSearchParams = new URLSearchParams();
        
 
        
        let myOption: RequestOptions = this.options;
        myOption.search = params;
        
    
        return this.http.post(this.api_uri_bill, body, this.options)
          .map((response: Response) => {
    
            
            let id=+response["_body"]
    
            //responseEntry=JSON.stringify(response["_body"]);//Object.assign(,responseEntry)
            if(id){
              return id;
            }else{
    
              return null;
    
            }
          })
          .catch(this.handleError);
      }
      public putInventoryDetail = (id_bill: number, body: BillDetailIDDTO): Observable<number | any> => {
        
            return this.http.put(this.api_uri_bill + "/" + id_bill, body, { headers: this.headers })
              .map((response: Response) => {
                let id=+response["_body"]
                if (id) {
                  return id;
                } else {
        
                  return null;
                }
              })
              .catch(this.handleError);
          }
        
          public deleteInventoryDetail = (id_bill?: number, id_inventory?: number): Observable<Response | any> => {
            let params: URLSearchParams = new URLSearchParams();
        
            let myOption: RequestOptions = this.options;
            myOption.search = params;
        
            return this.http.delete(this.api_uri_bill + '/' + id_bill, this.options)
              .map((response: Response) => {
                if (response) {
                  return true;
                } else {
        
                  return false;
                }
              })
              .catch(this.handleError);
          }
    

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

}
