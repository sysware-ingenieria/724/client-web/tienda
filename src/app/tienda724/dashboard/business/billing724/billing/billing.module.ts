import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/

import { BillGeneralComponent } from './bill-general/bill-general.component';
import { BillComponent } from './bill/bill.component';
import { DetailBillComponent } from './detail-bill/detail-bill.component';
import { BillDataComponent } from './bill-data/bill-data.component';
import { DialogQuantityBillComponent } from './dialog-quantity-bill/dialog-quantity-bill.component'

/*
************************************************
*     modules of  your app
*************************************************
*/

import { InventoriesModule } from '../../store724/inventories/inventories.module';

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { BillingService} from './billing.service'


import { InventoriesService } from '../../store724/inventories/inventories.service';
import { BarCodeService } from '../../store724/bar-codes/bar-code.service';
import { AttributeService } from '../../store724/attributes/attribute.service';
import { ThirdService } from '../../thirds724/third/third.service';
import { DocumentTypeService } from '../../thirds724/document-type/document-type.service';



import { BillShopComponent } from './bill-shop/bill-shop.component';
import { BillSaleComponent } from './bill-sale/bill-sale.component';
import { BillInputComponent } from './bill-input/bill-input.component';
import { BillOutputComponent } from './bill-output/bill-output.component';
import { BillReturnComponent } from './bill-return/bill-return.component';
import { BillRemissionComponent } from './bill-remission/bill-remission.component';
import { BillHeaderComponent } from './bill-header/bill-header.component';
import { BillDetailComponent } from './bill-detail/bill-detail.component';
import { BillDialogQuantityComponent } from './bill-dialog-quantity/bill-dialog-quantity.component';
import { BillThirdComponent } from './bill-third/bill-third.component';
import { BillDialogThirdComponent } from './bill-dialog-third/bill-dialog-third.component';
import { BillDocumentComponent } from './bill-document/bill-document.component';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    InventoriesModule,

    
  ],
  declarations: [BillGeneralComponent, BillComponent, DetailBillComponent, BillDataComponent, DialogQuantityBillComponent, BillShopComponent, BillSaleComponent, BillInputComponent, BillOutputComponent, BillReturnComponent, BillRemissionComponent, BillHeaderComponent, BillDetailComponent, BillDialogQuantityComponent, BillThirdComponent, BillDialogThirdComponent, BillDocumentComponent],
  entryComponents: [DialogQuantityBillComponent, BillDialogQuantityComponent, BillDialogThirdComponent, BillDocumentComponent],
  providers:[BillingService,InventoriesService, BarCodeService, AttributeService, ThirdService, DocumentTypeService]

})
export class BillingModule { }
