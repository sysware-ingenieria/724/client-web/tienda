import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { DetailPaymentComponent } from './detail-payment/detail-payment.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentDataComponent } from './payment-data/payment-data.component';

/*
************************************************
*     modules of  your app
*************************************************
*/


/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import {PaymentMethodService} from './payment-method.service'
/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  declarations: [DetailPaymentComponent, PaymentComponent, PaymentDataComponent],
  providers:[PaymentMethodService]
})
export class PaymentMethodModule { }
