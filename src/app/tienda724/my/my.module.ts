import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

/*
*************************************************
*     Principal component
*************************************************
*/
import { MyComponent } from './my/my.component';

/*
************************************************
*     modules of  your app
*************************************************
*/
import { MaterialModule } from '../../app.material';

import { ComponentsModule } from '../../components/components.module';

/*
*************************************************
*     services of  your app
*************************************************
*/
/*
*************************************************
*     models of  your app
*************************************************
*/
/*
*************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    MaterialModule,
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
   
      ],
  declarations: [MyComponent]
})
export class MyModule { }
