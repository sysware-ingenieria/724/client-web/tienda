import { Routes } from '@angular/router';

import { MyComponent } from './my/my.component';


export const MyRouting: Routes= [
  { path: 'my', component: null,
      children: [
        { path: '', redirectTo: 'getstarted', pathMatch: 'full'},
        { path: 'getstarted',   component: MyComponent }
       
      ]
  },

];