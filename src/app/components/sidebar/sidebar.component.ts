import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import {Router} from '@angular/router';


import { Menu } from '../../shared/menu';
import { Token } from '../../shared/token';
import { Person } from '../../shared/models/person';

import { LocalStorage } from '../../shared/localStorage';

import { AuthenticationService } from '../../authentication/authentication.service'
import { Third } from '../../tienda724/dashboard/business/thirds724/third/models/third';


declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES_START: RouteInfo[] = [
  { path: 'business/menu', title: 'Dashboard',  icon: 'dashboard', class: '' },
  { path: 'user-profile', title: 'User Profile',  icon:'person', class: '' }
];
export const ROUTES: RouteInfo[] = [
    { path: 'table-list', title: 'Table List',  icon:'content_paste', class: '' },
    { path: 'typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: 'icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    { path: 'maps', title: 'Maps',  icon:'location_on', class: '' },
    { path: 'notifications', title: 'Notifications',  icon:'notifications', class: '' },
    { path: 'upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
    menusLista:Menu[];
    token:Token;
    menuItems: any[];
    menuItemsStart: any[];
    person:Person;
    thirdFather:Third
    

  constructor( private authService:AuthenticationService,public locStorage: LocalStorage,
    private _router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItemsStart = ROUTES_START.filter(menuItem => menuItem);

      /** @todo elimanar la asiignación de token

    */
    
    
          let session=this.locStorage.getSession();
         if(!session){
           /**
           @todo Eliminar comentario para
           */
             this.Login();
         }else{
           this.menusLista=this.locStorage.getMenu();
           this.token=this.locStorage.getToken();
           
           this.person=this.locStorage.getPerson();
           this.thirdFather=this.locStorage.getThird();
    
         }
    
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };


  Logout() {
    this.locStorage.cleanSession();
    this.goIndex();
 
   }
 
   goIndex() {
     let link = ['/'];
     this._router.navigate(link);
   }
 
   goDashboard() {
     let link = ['/dashboard'];
     this._router.navigate(link);
 
   }
   Perfil() {
     alert("EN DESARROLLO");
 
   }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }
}
