import { Injectable } from '@angular/core';


//import { Token } from '../shared/token';
import { Third } from '../tienda724/dashboard/business/thirds724/third/models/third';
import { Session } from '../shared/session';
import { Person } from '../shared/models/person';
import { Token } from '../shared/token';
import { Menu } from '../shared/menu';
import { Rol } from '../shared/rol';


@Injectable()
export class LocalStorage {

  private session: Session;
  private person: Person;
  private token: Token;
  private menu: Menu[];
  private third:Third;
  private rol: Rol[];

  constructor() {
  if(localStorage.getItem('currentUserSessionStore724')){
      this.session = JSON.parse(localStorage.getItem('currentUserSessionStore724')) ;
  }


  if(localStorage.getItem('currentUserPersonStore724')){
    this.person = JSON.parse(localStorage.getItem('currentUserPersonStore724')) ;
  }
  if(localStorage.getItem('currentUserTokenStore724')){
      this.token = JSON.parse(localStorage.getItem('currentUserTokenStore724')) ;
  }

  if(localStorage.getItem('currentUserMenuStore724')){
      this.menu = JSON.parse(localStorage.getItem('currentUserMenusStore724')) ;
  }

  if(localStorage.getItem('currentUserRolStore724')){
       this.rol = JSON.parse(localStorage.getItem('currentUserRolStore724')) ;
  }

  if(localStorage.getItem('currentThirdFatherStore724')){
    this.rol = JSON.parse(localStorage.getItem('currentThirdFatherStore724')) ;
  }


  }

  getSession() {
    this.session= JSON.parse(localStorage.getItem('currentUserSessionStore724'));
    return this.session;
  }
   getToken() {
    this.token= JSON.parse(localStorage.getItem('currentUserTokenStore724'));
    return this.token;
  }
  getThird(){
    this.third= JSON.parse(localStorage.getItem('currentThirdFatherStore724'));
    return this.third;
  }

  getIdThird() {

    var id_third=localStorage.getItem('idThird724')

    return id_third ;
  }
  getTokenValue(){
    return '3020D4:0DD-2F413E82B-A1EF04559-78CA';
  }

  getUUID(){
    return JSON.parse(localStorage.getItem('UUIDThird724'));;
  }

  getPerson() {
    this.person= JSON.parse(localStorage.getItem('currentUserPersonStore724'));
    return this.person;
  }

  getMenu() {
    this.menu= JSON.parse(localStorage.getItem('currentUserMenuStore724'));
    return this.menu;
  }

  getRol() {
    this.rol= JSON.parse(localStorage.getItem('currentUserRolStore724'));
    return this.rol;
  }


  getIdPersonApp() {
    return (this.person['id_person'])?this.person['id_person']:null;
  }
  getUIdPersonApp() {
    return (this.person['uid_usuario'])?this.person['uid_usuario']:null;
  }

 getUser() {
    return (this.token['user'])?this.token['user']:null;
  }

  isSession() {
    if ( localStorage.getItem('currentUserSessionStore724') !== null ) {
      return true;
    }
    return false;
  }

  getIdApplication(){
    return 21;
  }  

  cleanSession() {
  
  localStorage.removeItem('currentThirdFatherStore724');
  localStorage.removeItem('currentUserSessionStore724');
  localStorage.removeItem('currentUserPersonStore724');
  localStorage.removeItem('currentUserTokenStore724');
  localStorage.removeItem('currentUserMenuStore724');
  localStorage.removeItem('currentUserRolStore724');
  localStorage.removeItem('currentUserStore724');
}

}
