import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

//import { MaterialModule } from '@angular/material';
import { MaterialModule } from './app.material';
import 'hammerjs';

/*
************************************************
*     modules of  your app
*************************************************
*/
 import {  WelcomeModule } from "./welcome/welcome.module";
 import {  AuthenticationModule } from "./authentication/authentication.module";
  import {  Tienda724Module } from "./tienda724/tienda724.module";


import { AppRouting } from './app.routing';


import { AppComponent } from './app.component';

import { ComponentsModule } from './components/components.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { BillDialogQuantityComponent } from './bill-dialog-quantity/bill-dialog-quantity.component';
import { BillDialogThirdComponent } from './bill-dialog-third/bill-dialog-third.component';

@NgModule({
  declarations: [
    AppComponent,
    BillDialogQuantityComponent,
    BillDialogThirdComponent

  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule.forRoot(AppRouting),
    MaterialModule,
    WelcomeModule,
    AuthenticationModule,
    Tienda724Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
